import { NavBarItemInterface } from '../interfaces/LayoutInterface';
import { getRoute } from 'constants/routes';
import { TFunction } from 'i18next';
import { SignInUpModalType } from 'components/layout/modal/full/SignInUpModal';
import { ReduxUser } from '../redux/reducers/userReducer';
import { Dispatch } from 'redux';
import { reduxUserDelete } from '../redux/actions/userActions';

export const getNavBarItems = (
  t: TFunction,
  toggleWithModal: (modalType?: SignInUpModalType) => void,
  user: ReduxUser,
  dispatch: Dispatch<any>
): NavBarItemInterface[] => [
  {
    url: getRoute('/'),
    label: t('pages.home'),
  },
  ...(user.token
    ? [
        {
          url: getRoute('/order'),
          label: t('pages.My orders'),
        },
        {
          onClick: () => {
            dispatch(reduxUserDelete());
            window.location.href = '/';
          },
          local: false,
          label: t('navBar.log out'),
        },
      ]
    : [
        {
          onClick: () => toggleWithModal(SignInUpModalType.SIGN_IN),
          local: false,
          label: t('navBar.logIn'),
        },
      ]),
];
