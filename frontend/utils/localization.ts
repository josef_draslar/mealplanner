import NextI18Next from 'next-i18next';
import { TFunction } from 'i18next';

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: 'en',
  defaultNS: 'common',
  localePath: 'public_data/locales',
  otherLanguages: [],
});

export const plurals = (t: TFunction, key: string, number: number): string => {
  return number === 0
    ? t(key + '-3', { number })
    : number === 1
    ? t(key + '-1', { number })
    : number >= 5
    ? t(key + '-3', { number })
    : t(key + '-2', { number });
};

export const useTranslation = () => ({
  t: (key: string): string => {
    const split = key.split('.');
    if (split.length > 0) {
      return split[split.length - 1];
    }
    return key;
  },
});

// export const useTranslation = () => {
//   const { t } = NextI18NextInstance.useTranslation();
//   return {
//     t: (
//       key: string | string[],
//       defaultValue?: string,
//       options?: { [key: string]: string } | string
//     ): string => {
//       const result = t(key, defaultValue, options);
//       const keyToCheck =
//         typeof key === 'string'
//           ? key
//           : key.length > 0
//           ? key[key.length - 1]
//           : null;
//
//       if (keyToCheck && keyToCheck === result) {
//         const split = keyToCheck.split('.');
//         if (split.length > 0) {
//           return split[split.length - 1];
//         }
//       }
//       return result;
//     },
//   };
// };

export const appWithTranslation = NextI18NextInstance.appWithTranslation;
