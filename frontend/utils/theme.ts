export enum ThemeEnum {
  'light' = 'light',
}

export const getThemeVars = (theme: ThemeEnum): string => {
  switch (theme) {
    case ThemeEnum.light:
      return convertThemeToString(lightTheme);
    default:
      return convertThemeToString(lightTheme);
  }
};

const convertThemeToString = (obj: { [key: string]: string }): string => {
  const keys: string[] = [];
  Object.keys(obj).forEach((key: string) => {
    keys.push(`--${key}:${obj[key]}`);
  });
  return keys.join(';');
};

const lightTheme = {
  textColor: '#000',
  titleColor: '#000',
  colorPrimary: '#3d60d3',
  colorPrimaryHover: '#2f4da2',
  colorSecondary: '#3dd374',
};
