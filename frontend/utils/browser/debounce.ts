let debounceTimer: NodeJS.Timer = null;

export const debounce = (time: number, fn: () => any) => {
  clearTimeout(debounceTimer);
  debounceTimer = setTimeout(fn, time);
};
