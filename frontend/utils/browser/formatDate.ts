export const formatDate = (dateStr: string): string => {
  const date = new Date(dateStr);
  return (
    date.getMonth() +
    '/' +
    date.getDate() +
    '/' +
    date.getFullYear() +
    ' ' +
    date.getHours() +
    ':' +
    date.getMinutes()
  );
};
