import { TFunction } from 'i18next';

export const validatePassword = (value: string, t: TFunction): string => {
  if (/[À-ž]/.test(value)) return t('Diacritics forbidden');
  if (!/[a-z]/.test(value)) return t('Missing small letter');
  if (!/[A-Z]/.test(value)) return t('Missing big letter');
  if (!/[0-9]/.test(value)) return t('Missing number');
  if (!/[\W|_]/.test(value))
    return t('Missing special character') + ' (!@#$%^&*()/?<>,.{[]}|\'~`"_)';
  if (value.length < 8) return t('Min length is 8 chars');
  if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(\W|_)).{8,}$/.test(value))
    return t('Password not valid');

  return undefined;
};
