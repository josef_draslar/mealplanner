const TOKEN_NAME = 'token';
const MAX_AGE = 30; // 30 days

export function setTokenCookie(token: string): void {
  setCookie(TOKEN_NAME, token);
}

export function removeTokenCookie(): void {
  deleteCookie(TOKEN_NAME);
}

export function getTokenCookie(): string | null {
  return getCookie(TOKEN_NAME);
}

export const getCookie = (cname: string): string => {
  const name = cname + '=';
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
};

export const setCookie = (
  name: string,
  value: string,
  expireInDays: number = MAX_AGE
) => {
  const d = new Date();
  d.setTime(d.getTime() + expireInDays * 24 * 60 * 60 * 1000);
  const expires = 'expires=' + d.toUTCString();
  document.cookie =
    name +
    '=' +
    value +
    ';' +
    expires +
    ';path=/;secure=' +
    (window.location.protocol === 'https:') +
    ';sameSite=lax';
};

export const deleteCookie = (name: string) => setCookie(name, '', -1);
