import axiosLib from 'axios';
import { getServerBaseUrl } from 'utils/getServerBaseUrl';
import { isBrowser } from 'utils/browser/isBrowser';

let axiosConfig = {
  baseURL: getServerBaseUrl() + '/api',
  timeout: 5000,
  headers: {
    token: isBrowser() ? localStorage.getItem('token') : undefined,
  },
};

export const resetAxiosConfigWithToken = (token: string): void => {
  axiosConfig = {
    ...axiosConfig,
    headers: {
      ...axiosConfig.headers,
      token,
    },
  };
  axios = createAxios();
};

const createAxios = () => {
  const axios = axiosLib.create(axiosConfig);
  axios.interceptors.request.use(
    function (config) {
      // Do something before request is sent
      return config;
    },
    function (error) {
      console.log('errores', error);
      // Do something with request error
      return Promise.reject(error);
    }
  );
  return axios;
};

export let axios = createAxios();
