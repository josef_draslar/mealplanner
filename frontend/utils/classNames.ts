interface ApplyClassInterface {
  [key: string]: boolean;
}

export const classNames = (
  ...args: (undefined | string | ApplyClassInterface)[]
): string => {
  const classes: string[] = [];
  args
    .filter((arg) => !!arg)
    .forEach((arg) => {
      if (typeof arg === 'string') {
        classes.push(arg);
      } else {
        Object.keys(arg as ApplyClassInterface).forEach((key) => {
          if ((arg as ApplyClassInterface)[key]) {
            classes.push(key);
          }
        });
      }
    });

  return classes.join(' ');
};
