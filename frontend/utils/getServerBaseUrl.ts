import axiosLib from 'axios';

export const getServerBaseUrl = (): string =>
  process.env.NODE_ENV !== 'production' ? 'http://localhost:7000' : '';
