import { NextApiRequestQuery } from 'next/dist/next-server/server/api-utils';
import {
  getUrlParamMessage,
  ToastInterface,
} from 'components/layout/toast/Toast';
import { getServerBaseUrl } from 'utils/getServerBaseUrl';

const Routes = [
  '/',
  '/order',
  '/server_pages/auth/facebook',
  '/server_pages/auth/google',
] as const;

export type RoutesType = typeof Routes[number];

export const getRoute = (
  routeE: RoutesType,
  params: string[] = [],
  toast?: ToastInterface,
  query?: NextApiRequestQuery
): string => {
  let route = routeE.toString();
  let lastReplacement = 0;
  if (params.length > 0) {
    params.forEach((value, index) => {
      const stringKey = `/{${index}}`;

      const idx = route.indexOf(stringKey, lastReplacement);

      if (idx !== -1) {
        route =
          route.substr(0, idx) +
          '/' +
          value +
          route.substr(idx + stringKey.length);

        lastReplacement = idx + ('' + value).length + 1;
      } else {
        console.error(
          `Such route parameter was not found. Route: ${route}, stringKey: ${stringKey}, value: ${value}, all params: ${JSON.stringify(
            params
          )}`
        );
      }
    });
  }

  const urlParams: string[] = [];

  if (toast) {
    urlParams.push(getUrlParamMessage(toast));
  }
  if (query) {
    Object.keys(query).map((key) => {
      urlParams.push(createUrlParam(key, query[key] as string));
    });
  }

  let urlParamsResult: string = urlParams.join('&');
  if (urlParamsResult) urlParamsResult = '?' + urlParamsResult;

  if (route.startsWith('/server_pages')) {
    route = getServerBaseUrl() + route;
  }

  return route + urlParamsResult;
};

export const createUrlParam = (key: string, value: string): string =>
  `${key}=${value}`;
