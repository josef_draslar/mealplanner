module.exports = {
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(ts)x?$/,
      },
      use: {
        loader: '@svgr/webpack'
      },
    });

    return config;
  },
};
