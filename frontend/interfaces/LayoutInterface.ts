export interface NavBarItemInterface {
  label: string;
  url?: string;
  local?: boolean;
  onClick?: () => void;
}
