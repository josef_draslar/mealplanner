import { OrdersPage } from 'components/pages/order/OrdersPage';
import { EnsureUserIsLoggedIn } from 'components/layout/ensureLoggedIn/EnsureUserIsLoggedIn';

export default function Home() {
  return (
    <EnsureUserIsLoggedIn>
      <OrdersPage />
    </EnsureUserIsLoggedIn>
  );
}
