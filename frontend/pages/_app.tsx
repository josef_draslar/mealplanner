import '../styles/globals.scss';
import { AppProps } from 'next/app';
import { useEffect } from 'react';
import { removeEmptyHashFromUrl } from 'utils/browser/removeEmptyHashFromUrl';
import { useStore } from '../redux/store';
import { Provider } from 'react-redux';
// import { appWithTranslation } from 'utils/localization';

function MyApp({ Component, pageProps }: AppProps) {
  const store = useStore(pageProps.initialReduxState);

  useEffect(() => {
    removeEmptyHashFromUrl();
  }, []);
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
// export default appWithTranslation(MyApp);
