import { useEffect } from 'react';
import { AxiosResponse } from 'axios';
import { HomePage } from 'components/pages/HomePage';
import { axios } from 'utils/axios';
import { getTokenCookie } from 'utils/browser/cookies';
import { ReduxUser } from '../redux/reducers/userReducer';
import { useDispatch, useSelector } from 'react-redux';
import { ReduxStore } from '../redux/reducers';
import { reduxUserSet } from '../redux/actions/userActions';

export default function Home() {
  const user: ReduxUser = useSelector((state: ReduxStore) => state.user);
  const dispatch = useDispatch();
  useEffect(() => {
    const token = getTokenCookie();
    if (token && user.token !== token) {
      dispatch(reduxUserSet(token));
    }
  }, []);
  return <HomePage />;
}
