import { RestaurantDetailPage } from 'components/pages/restaurant/RestaurantDetailPage';
import { EnsureUserIsLoggedIn } from 'components/layout/ensureLoggedIn/EnsureUserIsLoggedIn';

export default function RestaurantDetail() {
  return (
    <EnsureUserIsLoggedIn>
      <RestaurantDetailPage />
    </EnsureUserIsLoggedIn>
  );
}
