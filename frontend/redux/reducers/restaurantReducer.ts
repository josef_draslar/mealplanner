export enum RestaurantActionTypes {
  RESTAURANTS_SET = 'RESTAURANTS_SET',
}

export interface Meal {
  id: number;
  name: string;
  description: string;
  price: number;
}

export interface Restaurant {
  id: number;
  name: string;
  description: string;
  meals: Meal[];
}

export interface ReduxRestaurants {
  restaurants: Restaurant[];
}

const initialTimerState: () => ReduxRestaurants = () => ({
  restaurants: undefined,
});

export default (
  state = initialTimerState(),
  { type, payload }: { type: RestaurantActionTypes; payload: any }
) => {
  switch (type) {
    case RestaurantActionTypes.RESTAURANTS_SET:
      return {
        restaurants: payload,
      };
    default:
      return state;
  }
};
