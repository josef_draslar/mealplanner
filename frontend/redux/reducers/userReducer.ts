import { isBrowser } from 'utils/browser/isBrowser';

export enum UserActionTypes {
  USER_SET_TOKEN = 'USER_SET_TOKEN',
  USER_SET = 'USER_SET',
  USER_DELETE = 'USER_DELETE',
}

export interface ReduxUser {
  email: string;
  token: string;
  isOwner: boolean;
}

const initialTimerState: () => ReduxUser = () => ({
  email: undefined,
  token: isBrowser() ? localStorage.getItem('token') : undefined,
  isOwner: false,
});

export default (
  state = initialTimerState(),
  { type, payload }: { type: UserActionTypes; payload: any }
) => {
  switch (type) {
    case UserActionTypes.USER_SET_TOKEN:
      return {
        ...state,
        token: payload,
      };
    case UserActionTypes.USER_SET:
      return {
        ...state,
        email: payload.email,
        isOwner: payload.isOwner,
        restaurants: payload.restaurants,
        orders: payload.orders,
      };
    case UserActionTypes.USER_DELETE:
      return {
        ...initialTimerState(),
      };
    default:
      return state;
  }
};
