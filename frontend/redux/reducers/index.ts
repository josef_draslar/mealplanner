import { combineReducers } from 'redux';
import userReducer, { ReduxUser } from './userReducer';
import restaurantReducer, { ReduxRestaurants } from './restaurantReducer';

export interface ReduxStore {
  user: ReduxUser;
  restaurant: ReduxRestaurants;
}

const reducers = {
  user: userReducer,
  restaurant: restaurantReducer,
};

export default combineReducers(reducers);
