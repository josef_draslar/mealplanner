import { Dispatch } from 'redux';
import { axios } from 'utils/axios';
import { RestaurantActionTypes } from '../reducers/restaurantReducer';

export const reduxRestaurantsGet = () => async (dispatch: Dispatch) => {
  const restaurants = await axios.get('/restaurants');

  return dispatch({
    type: RestaurantActionTypes.RESTAURANTS_SET,
    payload: restaurants.data,
  });
};
