import { UserActionTypes } from '../reducers/userReducer';
import { Dispatch } from 'redux';
import { removeTokenCookie } from 'utils/browser/cookies';
import { axios, resetAxiosConfigWithToken } from 'utils/axios';
import { AxiosResponse } from 'axios';

interface UserApi {
  email: string;
  restaurants: any;
  orders: any;
  role: 'OWNER' | 'USER';
}

export const reduxUserSet = (token: string) => async (dispatch: Dispatch) => {
  localStorage.setItem('token', token);

  dispatch({
    type: UserActionTypes.USER_SET_TOKEN,
    payload: token,
  });
  resetAxiosConfigWithToken(token);

  await reduxUserFetchData()(dispatch);
};

export const reduxUserFetchData = () => async (dispatch: Dispatch) => {
  let responseUser: AxiosResponse<UserApi>;
  try {
    responseUser = await axios.get('/users/me');
  } catch (e) {
    return;
  }

  return dispatch({
    type: UserActionTypes.USER_SET,
    payload: {
      email: responseUser.data.email,
      restaurants: responseUser.data.restaurants,
      orders: responseUser.data.orders,
      isOwner: responseUser.data.role === 'OWNER',
    },
  });
};

export const reduxUserDelete = () => (dispatch: Dispatch) => {
  localStorage.removeItem('token');
  removeTokenCookie();
  return dispatch({
    type: UserActionTypes.USER_DELETE,
  });
};
