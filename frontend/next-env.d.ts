/// <reference types="next" />
/// <reference types="next/types/global" />

declare module '*.svg' {
  import React = require('react');
  const content: React.FC<React.SVGProps<SVGSVGElement>>;
  export const ReactComponent: React.SFC<React.SVGProps<SVGSVGElement>>;
  export default content;
}
