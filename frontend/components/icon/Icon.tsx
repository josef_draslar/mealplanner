import React, { ReactElement } from 'react';

interface Props {
  icon: React.FC<React.SVGProps<SVGSVGElement>>;
  className?: string;
}

const Icon = ({ icon: Icon, className }: Props): ReactElement => (
  <Icon className={className} />
);

export default Icon;
