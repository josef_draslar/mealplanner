import { ReactElement, useEffect, useState } from 'react';
import { AppLayout } from 'components/layout/AppLayout';
import { useTranslation } from 'utils/localization';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import H1 from 'components/typograpfy/H1';
import Button from 'components/basic/Button';
import { Meal, Restaurant } from 'redux/reducers/restaurantReducer';
import { ReduxStore } from 'redux/reducers';
import { ReduxUser } from 'redux/reducers/userReducer';
import { axios } from 'utils/axios';
import { MealCreation } from 'components/parts/meal/MealCreation';
import styles from './RestaurantDetailPage.module.scss';
import Cross from 'components/layout/modal/components/Cross';
import MealCreationModal from 'components/parts/meal/MealCreationModal';
import MealDeleteModal from 'components/parts/meal/MealDeleteModal';
import { RestaurantDelete } from 'components/parts/meal/RestaurantDelete';
import H3 from 'components/typograpfy/H3';
import { OrderCreate } from 'components/parts/order/OrderCreate';
import { RestaurantOrders } from 'components/parts/order/RestaurantOrders';
import RestaurantCreationModal from 'components/parts/restaurant/RestaurantCreationModal';
import { RestaurantCreation } from 'components/parts/restaurant/RestaurantCreation';

interface MealsToOrder {
  [id: number]: {
    price: number;
    amount: number;
  };
}

export const RestaurantDetailPage = (): ReactElement => {
  const { t } = useTranslation();
  const [deleteMealModalOpened, setDeleteMealModalOpened] = useState<number>(
    null
  );
  const [editMealModal, setEditMealModal] = useState<Meal>(null);
  const [meals, setMeals] = useState<Meal[]>(null);
  const [restaurant, setRestaurant] = useState<Restaurant>(null);
  const [orders, setOrders] = useState(null);
  const [canCreateMeals, setCanCreateMeals] = useState(null);
  const [mealsToOrder, setMealsToOrder] = useState<MealsToOrder>([]);
  const Router = useRouter();

  const addMealToOrder = (id: number, price: number) => {
    setMealsToOrder({
      ...mealsToOrder,
      [id]: {
        price: price,
        amount: mealsToOrder[id] ? mealsToOrder[id].amount + 1 : 1,
      },
    } as MealsToOrder);
  };

  const removeMealFromOrder = (id: number) => {
    const mealsToOrderLocal = { ...mealsToOrder };
    if (mealsToOrderLocal[id]?.amount > 1) {
      mealsToOrderLocal[id].amount -= 1;
    } else {
      delete mealsToOrderLocal[id];
    }
    setMealsToOrder({
      ...mealsToOrderLocal,
    } as MealsToOrder);
  };

  const user: ReduxUser = useSelector((state: ReduxStore) => state.user);

  useEffect(() => {
    if (Router.query.id) {
      fetchRestaurant();
      fetchRestaurantMeals();
      if (user?.isOwner) {
        axios
          .get(`/restaurants/${Router.query.id}/can_create_meals`)
          .then(() => setCanCreateMeals(true));
        axios
          .get(`/restaurants/${Router.query.id}/orders`)
          .then(({ data }) => setOrders(data));
      }
    }
  }, [Router, user?.isOwner]);

  const fetchRestaurant = () => {
    axios
      .get(`/restaurants/${Router.query.id}`)
      .then(({ data }) => setRestaurant(data));
  };

  const fetchRestaurantMeals = () => {
    axios
      .get(`/restaurants/${Router.query.id}/meals`)
      .then(({ data }) => setMeals(data));
  };

  let orderPrice = 0;
  Object.values(mealsToOrder).forEach(
    ({ amount, price }) => (orderPrice += amount * price)
  );

  return (
    <AppLayout title={restaurant ? restaurant.name : t('Restaurant')}>
      {restaurant && meals ? (
        <div>
          {deleteMealModalOpened && (
            <MealDeleteModal
              fetchMeals={fetchRestaurantMeals}
              mealId={deleteMealModalOpened}
              onHide={() => setDeleteMealModalOpened(null)}
            />
          )}
          {editMealModal && (
            <MealCreationModal
              fetchMeals={fetchRestaurantMeals}
              meal={editMealModal}
              onHide={() => setEditMealModal(null)}
              restaurantId={restaurant.id}
            />
          )}
          {canCreateMeals && (
            <>
              <MealCreation
                restaurantId={restaurant.id}
                fetchMeals={fetchRestaurantMeals}
              />
              <RestaurantDelete
                restaurantId={restaurant.id}
                fetchMeals={fetchRestaurantMeals}
              />
              <RestaurantCreation
                restaurant={restaurant}
                fetch={fetchRestaurant}
              />
            </>
          )}
          <H1>{restaurant.name}</H1>
          <div>{restaurant.description}</div>
          {orderPrice > 0 && (
            <div>
              <H3>{t('Order price:') + ' ' + orderPrice}</H3>
              <OrderCreate
                restaurantId={restaurant.id}
                mealsToOrder={Object.keys(mealsToOrder).map((key: string) => ({
                  id: Number(key),
                  amount: mealsToOrder[Number(key)].amount,
                }))}
              />
            </div>
          )}
          <div>
            {meals.map((meal, key) => {
              const orderingAmount = mealsToOrder[meal.id]?.amount || 0;
              return (
                <div key={key} className={styles.meal}>
                  {canCreateMeals ? (
                    <>
                      <Cross
                        cb={() => setDeleteMealModalOpened(meal.id)}
                        className={styles.cross}
                      />
                      <div onClick={() => setEditMealModal(meal)}>✎</div>
                    </>
                  ) : (
                    <></>
                  )}
                  <div>{meal.name}</div>
                  <div>{meal.description}</div>
                  <div>{meal.price}</div>
                  {!!orderingAmount && (
                    <>
                      <div>{t('Ordering: ') + orderingAmount}</div>
                      <Button onClick={() => removeMealFromOrder(meal.id)}>
                        -
                      </Button>
                    </>
                  )}
                  <Button onClick={() => addMealToOrder(meal.id, meal.price)}>
                    +
                  </Button>
                </div>
              );
            })}
          </div>
          {canCreateMeals && <RestaurantOrders restaurantId={restaurant.id} />}
        </div>
      ) : (
        <div>{t('is loading...')}</div>
      )}
    </AppLayout>
  );
};
