import { ReactElement, useEffect } from 'react';
import { AppLayout } from 'components/layout/AppLayout';
import { useTranslation } from 'utils/localization';
import { openSignInUpModal } from 'components/layout/navbar/NavBar';
import { SignInUpModalType } from 'components/layout/modal/full/SignInUpModal';
import { useRouter } from 'next/router';
import { deleteFromRoute } from 'components/layout/toast/Toast';
import { useDispatch, useSelector } from 'react-redux';
import { reduxRestaurantsGet } from 'redux/actions/restaurantActions';
import { ReduxStore } from 'redux/reducers';
import { Restaurant } from 'redux/reducers/restaurantReducer';
import H1 from 'components/typograpfy/H1';
import Button from 'components/basic/Button';
import { ReduxUser } from 'redux/reducers/userReducer';
import { RestaurantCreation } from 'components/parts/restaurant/RestaurantCreation';

export const HomePage = (): ReactElement => {
  const { t } = useTranslation();
  const Router = useRouter();
  const dispatch = useDispatch();
  const restaurants: Restaurant[] = useSelector(
    (state: ReduxStore) => state.restaurant.restaurants
  );
  const user: ReduxUser = useSelector((state: ReduxStore) => state.user);

  useEffect(() => {
    if (
      Router.query.open_modal &&
      Object.values(SignInUpModalType).includes(
        Router.query.open_modal as SignInUpModalType
      )
    ) {
      deleteFromRoute('open_modal', Router.query.open_modal as string);
      openSignInUpModal(Router.query.open_modal as SignInUpModalType);
    }
    dispatch(reduxRestaurantsGet());
  }, [Router, user?.token]);

  return (
    <AppLayout title={t('pages.Restaurants')}>
      <H1>{t('Choose restaurant & order meal')}</H1>
      {user.isOwner && <RestaurantCreation />}
      {restaurants ? (
        <div>
          {restaurants.map((restaurant, key) => (
            <div key={key}>
              <div>{restaurant.name}</div>
              <div>{restaurant.description}</div>
              <Button href={'/restaurant/' + restaurant.id}>
                {t('Choose')}
              </Button>
            </div>
          ))}
        </div>
      ) : (
        <div>loading...</div>
      )}
    </AppLayout>
  );
};
