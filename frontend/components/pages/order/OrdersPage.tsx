import { ReactElement, useEffect, useState } from 'react';
import { AppLayout } from 'components/layout/AppLayout';
import { useTranslation } from 'utils/localization';
import { openSignInUpModal } from 'components/layout/navbar/NavBar';
import { SignInUpModalType } from 'components/layout/modal/full/SignInUpModal';
import { useRouter } from 'next/router';
import { deleteFromRoute } from 'components/layout/toast/Toast';
import { useDispatch, useSelector } from 'react-redux';
import { reduxRestaurantsGet } from 'redux/actions/restaurantActions';
import { ReduxStore } from 'redux/reducers';
import { Restaurant } from 'redux/reducers/restaurantReducer';
import H1 from 'components/typograpfy/H1';
import Button from 'components/basic/Button';
import styles from './HomePage.module.scss';
import { ReduxUser } from 'redux/reducers/userReducer';
import { RestaurantCreation } from 'components/parts/restaurant/RestaurantCreation';
import { axios } from 'utils/axios';
import { OrderCancel } from 'components/parts/order/OrderCancel';
import { OrderReceived } from 'components/parts/order/OrderReceived';
import { formatDate } from 'utils/browser/formatDate';

export interface OrderInterface {
  id: number;
  total: number;
  status: string;
  placed: string;
  canceled: string;
  processing: string;
  in_route: string;
  delivered: string;
  received: string;
}

export const OrdersPage = (): ReactElement => {
  const { t } = useTranslation();
  const [orders, setOrders] = useState<OrderInterface[]>(null);
  const user: ReduxUser = useSelector((state: ReduxStore) => state.user);
  useEffect(() => {
    fetchOrders();
  }, [user?.token]);

  const fetchOrders = () => {
    axios.get(`/users/orders`).then(({ data }) => setOrders(data));
  };

  return (
    <AppLayout title={t('pages.Orders')}>
      <H1>{t('My orders')}</H1>
      {orders ? (
        <div>
          {orders.map((order: OrderInterface, key) => (
            <div key={key}>
              <div>{t('Total price') + ': ' + order.total}</div>
              <div>{t('Status') + ': ' + order.status}</div>
              {!!order.placed && (
                <div>{t('Placed at') + ': ' + formatDate(order.placed)}</div>
              )}
              {!!order.canceled && (
                <div>
                  {t('Canceled at') + ': ' + formatDate(order.canceled)}
                </div>
              )}
              {!!order.processing && (
                <div>
                  {t('Processing at') + ': ' + formatDate(order.processing)}
                </div>
              )}
              {!!order.in_route && (
                <div>
                  {t('In route at') + ': ' + formatDate(order.in_route)}
                </div>
              )}
              {!!order.delivered && (
                <div>
                  {t('Delivered at') + ': ' + formatDate(order.delivered)}
                </div>
              )}
              {!!order.received && (
                <div>
                  {t('Received at') + ': ' + formatDate(order.received)}
                </div>
              )}
              {order.status === 'PLACED' && (
                <OrderCancel fetch={fetchOrders} orderId={order.id} />
              )}
              {order.status === 'DELIVERED' && (
                <OrderReceived fetch={fetchOrders} orderId={order.id} />
              )}
            </div>
          ))}
        </div>
      ) : (
        <div>loading...</div>
      )}
    </AppLayout>
  );
};
