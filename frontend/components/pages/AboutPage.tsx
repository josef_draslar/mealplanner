import { ReactElement } from 'react';
import { AppLayout } from 'components/layout/AppLayout';
import { useTranslation } from 'utils/localization';

export const AboutPage = (): ReactElement => {
  const { t } = useTranslation();
  return (
    <AppLayout title={t('pages.home')}>
      <div>ABOUT PAGE</div>
    </AppLayout>
  );
};
