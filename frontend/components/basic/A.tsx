import { ReactChild, ReactElement } from 'react';
import Link from 'next/link';

interface Props {
  className?: string;
  children: ReactChild;
  local?: boolean;
  idx?: number;
  url?: string;
  onClick?: () => void;
}

const A = ({
  className,
  children,
  url,
  local,
  onClick,
  idx,
}: Props): ReactElement =>
  url ? (
    local ? (
      <Link key={idx} href={url}>
        <a onClick={onClick ? onClick : () => {}} className={className}>
          {children}
        </a>
      </Link>
    ) : (
      <a
        href={url}
        onClick={onClick ? onClick : () => {}}
        className={className}
      >
        {children}
      </a>
    )
  ) : (
    <a key={idx} onClick={onClick ? onClick : () => {}} className={className}>
      {children}
    </a>
  );

export default A;
