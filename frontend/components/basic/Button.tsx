import React, { MouseEvent, ReactChild, ReactElement } from 'react';
import ButtonLoader from 'components/loading/ButtonLoader';
import styles from './Button.module.scss';
import { classNames } from 'utils/classNames';
import A from 'components/basic/A';

export interface ButtonProps {
  children: ReactChild;
  isSubmitType?: boolean;
  isSecondary?: boolean;
  isTransparent?: boolean;
  anyWidth?: boolean;
  isWhite?: boolean;
  isDisabled?: boolean;
  isLoading?: boolean;
  className?: string;
  disabledReason?: string;
  error?: string;
  href?: string;
  onClick?: (e: MouseEvent) => void;
}

const Button = ({
  children,
  isSecondary,
  isTransparent,
  className,
  onClick,
  isSubmitType,
  disabledReason,
  error,
  isWhite,
  anyWidth,
  isDisabled,
  isLoading,
  href,
}: ButtonProps): ReactElement => {
  const styleType = isSecondary
    ? styles.secondary
    : isTransparent
    ? styles.transparent
    : isWhite
    ? styles.white
    : styles.primary;

  if (href) {
    return (
      <A
        className={classNames(
          styles.root,
          styleType,
          anyWidth ? styles.anyWidth : undefined,
          className,
          {
            [styles.disabled]: !!isDisabled || !!isLoading,
            [styles.marginBottom]: isDisabled !== undefined,
          }
        )}
        url={href}
        onClick={onClick as () => void}
      >
        {children}
      </A>
    );
  }

  return (
    <button
      className={classNames(
        styles.root,
        styleType,
        anyWidth ? styles.anyWidth : undefined,
        className,
        { [styles.disabled]: !!isDisabled || !!isLoading }
      )}
      onClick={onClick}
      type={isSubmitType ? 'submit' : 'button'}
      disabled={isDisabled || isLoading}
    >
      {!!disabledReason && (
        <span className={styles.disabledReason}>{disabledReason}</span>
      )}
      {!!error && <span className={styles.errorReason}>{error}</span>}
      <span>
        <span className={classNames({ [styles.hidden]: !!isLoading })}>
          {children}
        </span>
        <span className={styles.loader}>{isLoading && <ButtonLoader />}</span>
      </span>
    </button>
  );
};

export default Button;
