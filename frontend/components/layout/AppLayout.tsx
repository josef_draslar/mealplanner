import React, { ReactElement, ReactNode, useEffect } from 'react';
import Head from 'components/layout/Head';
import styles from './AppLayout.module.scss';
import Toast from 'components/layout/toast/Toast';
import { ELEMENT_ID_MODAL } from 'components/layout/modal/Modal';
import NavBar from 'components/layout/navbar/NavBar';
import { reduxUserFetchData } from '../../redux/actions/userActions';
import { useDispatch } from 'react-redux';

declare global {
  interface Window {
    pageRedirect: any;
  }
}

interface Props {
  title: string;
  children: ReactNode;
}

export const AppLayout = ({ title, children }: Props): ReactElement => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(reduxUserFetchData());
  }, []);

  return (
    <div>
      <Head title={title} />
      <Toast />
      <div id={ELEMENT_ID_MODAL} />
      <NavBar />
      <div className={styles.content}>{children}</div>
    </div>
  );
};
