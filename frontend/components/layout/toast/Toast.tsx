import React, { ReactElement, useEffect, useState } from 'react';
import { NextRouter, useRouter } from 'next/router';
import Icon from 'components/icon/Icon';
import InfoIcon from '../../../public_data/icons/info.svg';
import SuccessIcon from '../../../public_data/icons/check_circle.svg';
import WarningIcon from '../../../public_data/icons/alert.svg';
import DangerIcon from '../../../public_data/icons/alert_circle.svg';
import CancelIcon from '../../../public_data/icons/cancel.svg';
import { createUrlParam } from 'constants/routes';
import styles from './Toast.module.scss';
import { useTranslation } from 'utils/localization';
import { classNames } from 'utils/classNames';

export enum MessageType {
  INFO = 'INFO',
  DANGER = 'DANGER',
  WARNING = 'WARNING',
  SUCCESS = 'SUCCESS',
  DEFAULT = 'DEFAULT',
}

export const MessageKeys = {
  MESSAGE: 'message_key',
  TITLE: 'message_title',
  TYPE: 'message_type',
};

export const getUrlParamMessage = (message: ToastInterface): string => {
  let result = createUrlParam(MessageKeys.TYPE, message.messageType);
  if (message.messageKey) {
    result += '&' + createUrlParam(MessageKeys.MESSAGE, message.messageKey);
  }
  if (message.messageTitle) {
    result += '&' + createUrlParam(MessageKeys.TITLE, message.messageTitle);
  }
  return result;
};

const getIcon = (
  messageType: MessageType
): React.FC<React.SVGProps<SVGSVGElement>> => {
  switch (messageType) {
    case MessageType.INFO:
      return InfoIcon;
    case MessageType.SUCCESS:
      return SuccessIcon;
    case MessageType.WARNING:
      return WarningIcon;
    case MessageType.DANGER:
      return DangerIcon;
    default:
      return InfoIcon;
  }
};

const setCharAt = (str: string, idx: number, newChr: string): string => {
  return str.substring(0, idx) + newChr + str.substring(idx + 1);
};

const replaceResult = (result: string, key: string, value: string): string => {
  const replace =
    result.indexOf('&' + key + '=') !== -1
      ? '&' + key + '=' + value
      : result.indexOf(key + '=' + value + '&') !== -1
      ? key + '=' + value + '&'
      : result.indexOf('?' + key + '=') !== -1
      ? '?' + key + '=' + value
      : '';

  return result.replace(replace, '');
};

export const deleteFromRoute = (name: string, value: string) => {
  let result = window.location.search;
  result = replaceResult(result, name, encodeURIComponent(value) || '');

  if (result.charAt(1) === '&') {
    result = setCharAt(result, 1, '');
  }

  window.history.replaceState(
    window.history.state,
    '',
    `${window.location.pathname}${result}`
  );
};

const deleteMessageFromRoute = (toast: ToastInterface) => {
  let result = window.location.search;
  result = replaceResult(
    result,
    MessageKeys.MESSAGE,
    encodeURIComponent(toast.messageKey) || ''
  );
  result = replaceResult(result, MessageKeys.TYPE, toast.messageType);
  result = replaceResult(result, MessageKeys.TITLE, toast.messageTitle || '');

  if (result.charAt(1) === '&') {
    result = setCharAt(result, 1, '');
  }

  window.history.replaceState(
    window.history.state,
    '',
    `${window.location.pathname}${result}`
  );
};

const getMessageType = (type: string): MessageType => {
  if (type in MessageType) {
    return type as MessageType;
  }

  return MessageType.DEFAULT;
};

interface Props {
  dismissTime?: number;
}

let pushMessageInner: React.Dispatch<React.SetStateAction<ToastInterface | null>>;

export const pushMessage = (
  messageKey: string,
  messageType: MessageType,
  messageTitle?: string,
  translated?: boolean
): boolean => {
  if (pushMessageInner) {
    pushMessageInner({
      messageKey,
      messageTitle,
      messageType,
      translated,
    });
    return true;
  }

  return false;
};

export interface ToastInterface {
  messageKey?: string;
  messageTitle?: string;
  messageType: MessageType;
  translated?: boolean;
}

const getToastData = (Router: NextRouter): ToastInterface | null => {
  const toastData: ToastInterface = {
    messageKey: Router.query.message_key as string,
    messageTitle: Router.query.message_title as string,
    messageType: Router.query.message_type as MessageType,
  };

  if (
    (toastData.messageKey || toastData.messageTitle) &&
    toastData.messageType
  ) {
    return toastData;
  }

  return null;
};

const Toast = ({ dismissTime = 10000 }: Props): ReactElement => {
  const { t } = useTranslation();
  const Router = useRouter();
  const [toastData, setToastData] = useState<ToastInterface>(null);

  pushMessageInner = setToastData;

  useEffect(() => {
    const toastData = getToastData(Router);
    if (toastData && dismissTime) {
      setToastData(toastData);
      deleteMessageFromRoute(toastData);
      const interval = setInterval(() => {
        deleteToast();
      }, dismissTime);

      return () => {
        clearInterval(interval);
      };
    }
  }, [Router]);

  const deleteToast = () => {
    setToastData(null);
  };

  const getToastTranslation = (key: string): string => {
    const keyFull = 'Toast.' + key;
    const translation: string = t(keyFull) as string;
    if (translation === keyFull) {
      return t('Toast.generalError');
    } else if (key.indexOf(' ') !== -1) {
      return key.replace('_message', '').replace('_title', '');
    } else {
      return translation;
    }
  };

  return (
    <div
      className={classNames(styles.notificationContainer, styles.topCenter, {
        [styles.hidden]: !toastData,
      })}
    >
      {!!toastData && (
        <div
          className={classNames(
            styles.notification,
            styles.toast,
            styles.topCenter,
            styles[getMessageType(toastData.messageType)]
          )}
        >
          <button className={styles.delete} onClick={() => deleteToast()}>
            <Icon icon={CancelIcon} />
          </button>
          <div className={styles.notificationImage}>
            <Icon icon={getIcon(toastData.messageType)} />
          </div>
          <div>
            <p
              className={classNames(styles.notificationTitle, {
                [styles.withBorder]: !!toastData.messageKey,
              })}
            >
              {toastData.messageTitle
                ? toastData.translated
                  ? toastData.messageTitle
                  : getToastTranslation(toastData.messageTitle)
                : ''}
            </p>
            <p className={styles.notificationMessage}>
              {toastData.messageKey
                ? toastData.translated
                  ? toastData.messageKey
                  : getToastTranslation(toastData.messageKey)
                : ''}
            </p>
          </div>
        </div>
      )}
    </div>
  );
};

export default Toast;
