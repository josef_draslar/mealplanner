import React, { useEffect, useState } from 'react';
import styles from './TopProgressBar.module.scss';

const TopProgressBar = () => {
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    //@ts-ignore-next-line
    window.pageRefresh = (event: MouseEvent) => {
      if (
        event.ctrlKey ||
        event.shiftKey ||
        event.metaKey || // apple
        (event.button && event.button == 1) // middle mouse button click
      )
        return;
      setLoading(true);
    };
    window.pageRedirect = (url: string) => {
      console.log('redirecting to: ' + url);
      setLoading(true);
      window.location.href = url;
    };
  }, []);

  if (!loading) return null;

  return (
    <div className={styles.loadingBar}>
      <div />
    </div>
  );
};

export default TopProgressBar;
