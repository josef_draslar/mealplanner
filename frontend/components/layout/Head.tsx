import React, { ReactElement } from 'react';
import NextHead from 'next/head';
import { getThemeVars, ThemeEnum } from 'utils/theme';

interface Props {
  title: string;
  theme?: ThemeEnum;
}

const Head = ({ title, theme = ThemeEnum.light }: Props): ReactElement => (
  <NextHead>
    <title>{title}</title>
    <meta
      name="google-site-verification"
      content="Aa0NLMk3UGygs2LpcO2nhC8ghqtC_oSneWJhYDEOVt8"
    />
    <link rel="icon" href="/static/favicon.ico" />
    <link rel="shortcut icon" href="/favicon@2x.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/touch-icon-iphone.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/touch-icon-ipad.png" />
    <link
      rel="apple-touch-icon"
      sizes="180x180"
      href="/touch-icon-iphone-retina.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="167x167"
      href="/touch-icon-ipad-retina.png"
    />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>{`:root {${getThemeVars(theme)}}`}</style>
  </NextHead>
);

export default Head;
