import { ReactElement } from 'react';
import { NavBarItem } from './NavBarItem';
import { NavBarItemInterface } from 'interfaces/LayoutInterface';
import styles from './NavBarDesktop.module.scss';

interface Props {
  items: NavBarItemInterface[];
  currentLocation: string;
}

export const NavBarDesktop = ({
  items,
  currentLocation,
}: Props): ReactElement => {
  return (
    <div className={styles.wrapper}>
      {items.map((item, idx) => (
        <NavBarItem
          key={idx}
          item={item}
          idx={idx}
          currentLocation={currentLocation}
        />
      ))}
    </div>
  );
};
