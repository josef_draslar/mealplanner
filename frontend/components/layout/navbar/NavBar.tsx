import React, { useEffect, useMemo, useState } from 'react';
import Link from 'next/link';
import { classNames } from 'utils/classNames';
import SignInUpModal, {
  SignInUpModalType,
} from 'components/layout/modal/full/SignInUpModal';
import { getRoute } from 'constants/routes';
import Logo from '../../../public_data/vercel.svg';
import Icon from 'components/icon/Icon';
import { NavBarDesktop } from 'components/layout/navbar/NavBarDesktop';
import { NavBarItemInterface } from '../../../interfaces/LayoutInterface';
import { getNavBarItems } from 'utils/getNavBarItems';
import { useTranslation } from 'utils/localization';
import { useRouter } from 'next/router';
import NavBarMobile from 'components/layout/navbar/NavBarMobile';
import { isBrowser } from 'utils/browser/isBrowser';
import styles from './NavBar.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { ReduxUser } from '../../../redux/reducers/userReducer';
import { ReduxStore } from '../../../redux/reducers';

const isSmScreen = (): boolean => document.documentElement.clientWidth < 768;

interface State {
  isOpen: boolean;
  fixed: boolean;
  isSmScreen: boolean;
  modalType?: SignInUpModalType;
}

export let openSignInUpModal: (modalType: SignInUpModalType) => void;

const NavBar = () => {
  if (!isBrowser()) return null;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const user: ReduxUser = useSelector((state: ReduxStore) => state.user);
  const Router = useRouter();
  const [state, setState] = useState<State>({
    isOpen: false,
    fixed: false,
    isSmScreen: isSmScreen(),
    modalType: undefined,
  } as State);

  openSignInUpModal = (modalType: SignInUpModalType) =>
    setState({ ...state, modalType });

  useEffect(() => {
    document.addEventListener('scroll', scroll);
    window.addEventListener('resize', onWindowResize);

    return () => {
      document.removeEventListener('scroll', scroll);
    };
  }, [state]);

  const onWindowResize = () => {
    if (state.isSmScreen !== isSmScreen()) {
      setState({
        ...state,
        isSmScreen: !state.isSmScreen,
      });
    }
  };

  const scroll = () => {
    const browserHeight =
      (isNaN(window.innerHeight)
        ? document.documentElement.clientHeight
        : window.innerHeight) || 0;
    const scrollFromTop =
      document.body.scrollTop || document.documentElement.scrollTop;
    const { fixed } = state;

    const comparison = scrollFromTop - browserHeight;

    if (comparison > 0 && !fixed) {
      setState({ ...state, fixed: true });
    } else if (comparison <= 0 && fixed) {
      setState({ ...state, fixed: false });
    }
  };

  const toggle = () => {
    setState({
      ...state,
      isOpen: !state.isOpen,
    });
  };

  const toggleWithModal = (modalType?: SignInUpModalType) => {
    setState({
      ...state,
      modalType,
      isOpen: false,
    });
  };

  const items: NavBarItemInterface[] = getNavBarItems(
    t,
    toggleWithModal,
    user,
    dispatch
  );

  const currentLocation = Router.pathname;

  return (
    <div>
      {!!state.modalType && (
        <SignInUpModal
          onHide={() => toggleWithModal()}
          type={state.modalType}
        />
      )}
      <div>
        <div
          className={classNames(styles.navBar, {
            [styles.navBarFixed]: state.fixed,
          })}
        >
          <div className={styles.container}>
            <div className={styles.navBarContent}>
              <Link href={getRoute('/')}>
                <a className={styles.brand}>
                  <Icon icon={Logo} />
                </a>
              </Link>
              <NavBarDesktop items={items} currentLocation={currentLocation} />
            </div>
            <div
              className={classNames(styles.menuBars, {
                [styles.menuBarsOpened]: state.isOpen,
              })}
              onClick={toggle}
            >
              <div />
              <div />
              <div />
            </div>
            <NavBarMobile
              toggleWithModal={toggleWithModal}
              toggle={toggle}
              items={items}
              isOpen={state.isOpen}
              currentLocation={currentLocation}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
