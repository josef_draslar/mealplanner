import React, { ReactElement } from 'react';
import styles from './NavBarMobile.module.scss';
import { SignInUpModalType } from 'components/layout/modal/full/SignInUpModal';
import { classNames } from 'utils/classNames';
import { useTranslation } from 'utils/localization';
import { NavBarItem } from 'components/layout/navbar/NavBarItem';
import { NavBarItemInterface } from '../../../interfaces/LayoutInterface';

interface Props {
  isOpen: boolean;
  toggle: () => void;
  toggleWithModal: (type?: SignInUpModalType) => void;
  items: NavBarItemInterface[];
  currentLocation: string;
}

const NavMobile = ({
  isOpen,
  toggle,
  items,
  currentLocation,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const reloadPage = () => window.location.reload();
  /*
  todo use this
  const openSignIn = () => toggleWithModal(SignInUpModalType.SIGN_IN);

  const openSignUp = () => toggleWithModal(SignInUpModalType.SIGN_UP);

  const openSignUp = () => toggleWithModal(SignInUpModalType.SIGN_UP);

  const logOutUser = () => {
    toggle();
    UserService.logOutUser();
  };

   */

  return (
    <div>
      <div
        className={classNames(styles.backDrop, {
          [styles.backDropOpened]: isOpen,
        })}
        onClick={toggle}
      />
      <div
        className={classNames(styles.collapsible, {
          [styles.collapsibleOpened]: isOpen,
        })}
        onClick={(e): void => e.stopPropagation()}
      >
        <div
          className={classNames(styles.crossBars, {
            [styles.crossBarsClosed]: !isOpen,
          })}
          onClick={toggle}
        >
          <div />
          <div />
        </div>
        <div className={styles.content}>
          <div className={styles.title}>{t('Menu')}</div>
          {items.map((item, idx) => (
            <NavBarItem
              key={idx}
              item={item}
              idx={idx}
              currentLocation={currentLocation}
              isMobile
            />
          ))}
        </div>
        <div className={styles.reloadPage} onClick={reloadPage}>
          {t('Reload page')}
        </div>
      </div>
    </div>
  );
};

export default NavMobile;
