import { ReactElement } from 'react';
import Link from 'next/link';
import { NavBarItemInterface } from 'interfaces/LayoutInterface';
import styles from './NavBarItem.module.scss';
import A from 'components/basic/A';

interface Props {
  item: NavBarItemInterface;
  isMobile?: boolean;
  idx: number;
  currentLocation: string;
}

export const NavBarItem = ({
  item: { label, url, onClick, local = true },
  isMobile,
  currentLocation,
  idx,
}: Props): ReactElement => {
  const className: string = isMobile
    ? styles.navBarItemMobile
    : styles.navBarItem;
  return (
    <A
      className={className}
      onClick={onClick}
      idx={idx}
      local={local}
      url={url}
    >
      {label}
    </A>
  );
};
