import { ReactChild, ReactElement, useEffect } from 'react';
import { ReduxUser } from 'redux/reducers/userReducer';
import { useSelector } from 'react-redux';
import { ReduxStore } from 'redux/reducers';
import { useRouter } from 'next/router';

interface Props {
  children: ReactChild;
}

export const EnsureUserIsLoggedIn = ({ children }: Props): ReactElement => {
  const user: ReduxUser = useSelector((state: ReduxStore) => state.user);
  const Router = useRouter();

  useEffect(() => {
    if (!user.token) {
      Router.push('/?open_modal=SIGN_IN');
    }
  }, [Router]);
  return <>{children}</>;
};
