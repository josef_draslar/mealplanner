import React, { ReactElement } from 'react';
import styles from './Cross.module.scss';
import { classNames } from 'utils/classNames';

interface Props {
  cb?: () => void;
  className?: string;
}

const Cross = ({ cb, className }: Props): ReactElement => (
  <div className={classNames(styles.cross, className)} onClick={cb}>
    <div />
    <div />
  </div>
);

export default Cross;
