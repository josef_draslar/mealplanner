import { KeyboardEvent, ReactElement, ReactNode, useEffect } from 'react';
import ReactDOM from 'react-dom';
import styles from './Modal.module.scss';
import { isBrowser } from 'utils/browser/isBrowser';
import Cross from 'components/layout/modal/components/Cross';
import H3 from 'components/typograpfy/H3';

export const ELEMENT_ID_MODAL = 'modal';

enum ModalSizeEnum {
  'ALERT' = 'ALERT',
  'FULL' = 'FULL',
  'XXL' = 'XXL',
  'XL' = 'XL',
  'L' = 'L',
  'M' = 'M',
  'S' = 'S',
}

interface Props {
  children: ReactNode;
  onHide: () => void;
  title?: string;
  size?: ModalSizeEnum;
}

const getModalElement = (): HTMLElement =>
  document.getElementById(ELEMENT_ID_MODAL) as HTMLElement;

const Modal = ({
  children,
  onHide,
  size = ModalSizeEnum.L,
  title,
}: Props): ReactElement => {
  useEffect(() => {
    document.body.classList.add(styles.noScroll);
    return () => {
      document.body.classList.remove(styles.noScroll);
    };
  }, []);

  const onKeyDown = (e: KeyboardEvent<HTMLDivElement>) =>
    e.keyCode === 27 && onHide();

  return (
    <>
      {isBrowser() &&
        ReactDOM.createPortal(
          <div className={styles.modal} onKeyDown={onKeyDown}>
            <div className={styles.overlay} onClick={onHide} />
            <div className={styles['cardWrapper-' + size]} onClick={onHide}>
              <div className={styles.card} onClick={(e) => e.stopPropagation()}>
                <div className={styles.header}>
                  {title && (
                    <div className={styles.title}>
                      <H3 center>{title}</H3>
                    </div>
                  )}
                  <div className={styles.crossBlock}>
                    <Cross cb={onHide} className={styles.cross} />
                  </div>
                </div>
                {children}
              </div>
            </div>
          </div>,
          getModalElement()
        )}
    </>
  );
};

export default Modal;
