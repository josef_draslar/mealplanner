import { ReactElement, useState } from 'react';
import Modal from 'components/layout/modal/Modal';
import { useTranslation } from 'utils/localization';
import styles from './SignInUpModal.module.scss';
import Socials from 'components/parts/sign_in_up/Socials';
import SignInUpForm from 'components/parts/sign_in_up/SignInUpForm';
import ResetPasswordForm from 'components/parts/sign_in_up/ResetPasswordForm';
import SetNewPasswordForm from 'components/parts/sign_in_up/SetNewPasswordForm';

export enum SignInUpModalType {
  SIGN_IN = 'SIGN_IN',
  SIGN_UP = 'SIGN_UP',
  FORGET_PASSWORD = 'FORGET_PASSWORD',
  RESET_PASSWORD = 'RESET_PASSWORD',
}

interface Props {
  onHide: () => void;
  type?: SignInUpModalType;
}

const SignInUpModal = ({
  onHide,
  type: typeProps = SignInUpModalType.SIGN_IN,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const [type, setType] = useState<SignInUpModalType>(typeProps);

  return (
    <Modal
      onHide={onHide}
      title={t(
        type === SignInUpModalType.SIGN_IN
          ? 'Log in'
          : type === SignInUpModalType.SIGN_UP
          ? 'Sign up'
          : type === SignInUpModalType.RESET_PASSWORD
          ? 'Set new password'
          : 'Password reset'
      )}
    >
      <div className={styles.content}>
        {type === SignInUpModalType.SIGN_IN ? (
          <>
            <Socials />
            <SignInUpForm onHide={onHide} type={type} />
          </>
        ) : type === SignInUpModalType.SIGN_UP ? (
          <>
            <Socials />
            <SignInUpForm onHide={onHide} type={type} />
          </>
        ) : type === SignInUpModalType.RESET_PASSWORD ? (
          <SetNewPasswordForm onHide={onHide} />
        ) : (
          <ResetPasswordForm onHide={onHide} />
        )}
        <div className={styles.bottom}>
          <div className={styles.bottomInner}>
            <div>
              {t(
                type === SignInUpModalType.SIGN_IN
                  ? 'Do not have an account?'
                  : type === SignInUpModalType.SIGN_UP
                  ? 'Already have an account?'
                  : ''
              )}{' '}
              <span
                className={styles.switchLink}
                onClick={() =>
                  setType(
                    type === SignInUpModalType.SIGN_IN
                      ? SignInUpModalType.SIGN_UP
                      : type === SignInUpModalType.SIGN_UP
                      ? SignInUpModalType.SIGN_IN
                      : SignInUpModalType.SIGN_IN
                  )
                }
              >
                {t(
                  type === SignInUpModalType.SIGN_IN
                    ? 'Sign up'
                    : type === SignInUpModalType.SIGN_UP
                    ? 'Log in'
                    : 'Back'
                )}
              </span>
            </div>
            <div>
              {type === SignInUpModalType.SIGN_IN && (
                <>
                  {t('Forget password?')}{' '}
                  <span
                    className={styles.switchLink}
                    onClick={() => setType(SignInUpModalType.FORGET_PASSWORD)}
                  >
                    {t('Reset password')}
                  </span>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default SignInUpModal;
