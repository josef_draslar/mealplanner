import { ReactNode } from 'react';

const ModalDOMElement = (): ReactNode => <div />;

export default ModalDOMElement;
