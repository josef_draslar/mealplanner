import { ReactElement } from 'react';
import { useTranslation } from 'utils/localization';
import styles from './Input.module.scss';
import { debounce } from 'utils/browser/debounce';
import { TFunction } from 'i18next';

interface Props {
  label: string;
  error?: string;
  name: string;
  autoComplete?: string;
  value: string;
  onChange: (name: string, value: string) => void;
  setFormError?: (name: string, value: string) => void;
  validateCb?: (value: string, t: TFunction) => string;
}

const TextArea = ({
  label,
  name,
  value,
  error,
  onChange,
  setFormError,
  validateCb,
  autoComplete,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const onChangeLocal = (name: string, value: string) => {
    onChange(name, value);
    if (validateCb) {
      debounce(500, () => {
        setFormError && setFormError(name, validateCb(value, t));
      });
    }
  };

  return (
    <div className={styles.wrapper}>
      <label>{label}</label>
      <textarea
        name={name}
        className={styles.input}
        autoComplete={autoComplete}
        value={value}
        onChange={(e) => onChangeLocal(e.target.name, e.target.value)}
      />
      {error && <div className={styles.error}>{error}</div>}
    </div>
  );
};

export default TextArea;
