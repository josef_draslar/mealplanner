import { ReactElement } from 'react';
import { useTranslation } from 'utils/localization';
import styles from './Input.module.scss';
import { debounce } from 'utils/browser/debounce';
import { TFunction } from 'i18next';

interface Props {
  label: string;
  error?: string;
  name: string;
  type: 'email' | 'password' | 'text' | 'number';
  autoComplete?: string;
  value: string | number;
  onChange: (name: string, value: string) => void;
  setFormError?: (name: string, value: string) => void;
  validateCb?: (value: string, t: TFunction) => string;
  min?: number;
}

const Input = ({
  label,
  name,
  value,
  type,
  error,
  onChange,
  setFormError,
  validateCb,
  autoComplete,
  min,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const onChangeLocal = (name: string, value: string) => {
    onChange(name, value);
    if (validateCb) {
      debounce(500, () => {
        setFormError && setFormError(name, validateCb(value, t));
      });
    }
  };

  return (
    <div className={styles.wrapper}>
      <label>{label}</label>
      <input
        name={name}
        className={styles.input}
        type={type}
        autoComplete={autoComplete}
        value={value}
        onChange={(e) => onChangeLocal(e.target.name, e.target.value)}
        min={min}
      />
      {error && <div className={styles.error}>{error}</div>}
    </div>
  );
};

export default Input;
