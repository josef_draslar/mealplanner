import React, { ReactElement } from 'react';
import styles from './H2.module.scss';
import { classNames } from 'utils/classNames';

interface Props {
  children?: React.ReactChild;
  className?: string;
}

const H2 = (props: Props): ReactElement => {
  const { children, className } = props;

  return <h2 className={classNames(styles.root, className)}>{children}</h2>;
};

export default H2;
