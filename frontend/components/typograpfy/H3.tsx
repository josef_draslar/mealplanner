import React, { ReactElement } from 'react';
import styles from './H3.module.scss';
import { classNames } from 'utils/classNames';

interface Props {
  children?: React.ReactChild;
  className?: string;
  center?: boolean;
}

const H3 = ({ children, className, center }: Props): ReactElement => (
  <h3
    className={classNames(styles.root, className, { [styles.center]: center })}
  >
    {children}
  </h3>
);

export default H3;
