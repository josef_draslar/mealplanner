import React, { ReactElement } from 'react';
import styles from './H1.module.scss';
import { classNames } from 'utils/classNames';

interface Props {
  children?: React.ReactChild;
  className?: string;
}

const H1 = ({ children, className }: Props): ReactElement => (
  <h1 className={classNames(styles.root, className)}>{children}</h1>
);

export default H1;
