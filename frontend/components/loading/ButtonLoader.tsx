import React, { ReactElement } from 'react';
import styles from './ButtonLoader.module.scss';
import { classNames } from 'utils/classNames';

interface Props {
  isDark?: boolean;
}

const ButtonLoader = ({ isDark }: Props): ReactElement => (
  <div className={classNames(styles.root, { [styles.isDark]: !!isDark })}>
    <div />
    <div />
    <div />
    <div />
  </div>
);

export default ButtonLoader;
