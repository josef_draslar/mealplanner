import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import styles from './RestaurantCreationForm.module.scss';
import Button from 'components/basic/Button';
import { axios } from 'utils/axios';
import Input from 'components/form/Input';
import { MessageType, pushMessage } from 'components/layout/toast/Toast';
import { useDispatch } from 'react-redux';
import { reduxRestaurantsGet } from '../../../redux/actions/restaurantActions';
import TextArea from 'components/form/TextArea';
import { Restaurant } from 'redux/reducers/restaurantReducer';

interface Props {
  onHide: () => void;
  fetch?: () => void;
  restaurant?: Restaurant;
}

interface Form {
  name: string;
  description: string;
}

const RestaurantCreationForm = ({
  onHide,
  fetch,
  restaurant,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState<boolean | string>(false);
  const [form, setForm] = useState<Form>({
    name: restaurant?.name || '',
    description: restaurant?.description || '',
  });
  const { name, description } = form;
  const updateForm = (name: string, value: string) => {
    setForm({ ...form, [name]: value });
  };

  const onSubmit = async () => {
    setLoading(true);
    if (restaurant) {
      try {
        await axios.put('/restaurants/' + restaurant.id, {
          name,
          description,
        });
        pushMessage('Restaurant updated.', MessageType.SUCCESS);

        fetch && fetch();
        onHide();
      } catch ({ response }) {
        setLoading(t('Something went wrong, try that later please'));
      }
    } else {
      try {
        await axios.post('/restaurants', {
          name,
          description,
        });
        pushMessage('Restaurant created.', MessageType.SUCCESS);

        dispatch(reduxRestaurantsGet());
        onHide();
      } catch ({ response }) {
        setLoading(t('Something went wrong, try that later please'));
      }
    }
  };

  const disabledReason = !name
    ? t('Fill name')
    : !description
    ? t('Fill description')
    : undefined;

  return (
    <div className={styles.half}>
      <Input
        label={t('Name')}
        name="name"
        type="text"
        value={name}
        onChange={updateForm}
      />
      <TextArea
        label={t('Description')}
        name="description"
        value={description}
        onChange={updateForm}
      />
      <Button
        onClick={onSubmit}
        isDisabled={!!disabledReason}
        disabledReason={disabledReason}
        isLoading={typeof isLoading === 'string' ? false : isLoading}
        error={typeof isLoading === 'string' ? isLoading : null}
      >
        {t(restaurant ? 'Save' : 'Create')}
      </Button>
    </div>
  );
};

export default RestaurantCreationForm;
