import { ReactElement, useState } from 'react';
import Modal from 'components/layout/modal/Modal';
import { useTranslation } from 'utils/localization';
import styles from './RestaurantCreationModal.module.scss';
import RestaurantCreationForm from 'components/parts/restaurant/RestaurantCreationForm';
import { Restaurant } from 'redux/reducers/restaurantReducer';

interface Props {
  onHide: () => void;
  fetch?: () => void;
  restaurant?: Restaurant;
}

const RestaurantCreationModal = ({
  onHide,
  fetch,
  restaurant,
}: Props): ReactElement => {
  const { t } = useTranslation();

  return (
    <Modal
      onHide={onHide}
      title={t(restaurant ? 'Edit restaurant' : 'Create restaurant')}
    >
      <div className={styles.content}>
        <RestaurantCreationForm
          restaurant={restaurant}
          fetch={fetch}
          onHide={onHide}
        />
      </div>
    </Modal>
  );
};

export default RestaurantCreationModal;
