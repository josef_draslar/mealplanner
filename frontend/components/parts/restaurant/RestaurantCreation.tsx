import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import Button from 'components/basic/Button';
import RestaurantCreationModal from 'components/parts/restaurant/RestaurantCreationModal';
import { Restaurant } from 'redux/reducers/restaurantReducer';

interface Props {
  restaurant?: Restaurant;
  fetch?: () => void;
}

export const RestaurantCreation = ({
  restaurant,
  fetch,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const [isModalOpened, setModalOpened] = useState(false);

  return (
    <>
      <Button onClick={() => setModalOpened(true)}>
        {t(restaurant ? 'Update restaurant' : 'Create restaurant')}
      </Button>
      {isModalOpened && (
        <RestaurantCreationModal
          restaurant={restaurant}
          fetch={fetch}
          onHide={() => setModalOpened(false)}
        />
      )}
    </>
  );
};
