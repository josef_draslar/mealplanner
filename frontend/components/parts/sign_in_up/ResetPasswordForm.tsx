import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import styles from './ResetPasswordForm.module.scss';
import Button from 'components/basic/Button';
import { axios } from 'utils/axios';
import Input from 'components/form/Input';
import { MessageType, pushMessage } from 'components/layout/toast/Toast';

interface Props {
  onHide: () => void;
}

interface Form {
  email: string;
}

const initStateFormErrors: Form = {
  email: null,
};

const ResetPasswordForm = ({ onHide }: Props): ReactElement => {
  const { t } = useTranslation();
  const [isLoading, setLoading] = useState<boolean | string>(false);
  const [form, setForm] = useState<Form>({
    email: '',
  });
  const [formErrors, setFormErrors] = useState<Form>(initStateFormErrors);
  const { email } = form;
  const updateForm = (name: string, value: string) => {
    setForm({ ...form, [name]: value });
  };
  const setFormError = (name: string, value: string) => {
    setFormErrors({ ...formErrors, [name]: value });
  };

  const onSubmit = async () => {
    setLoading(true);
    try {
      const response = await axios.post('/auth/reset-password', {
        email,
      });
      pushMessage(
        'Check your email and reset password using sent link.',
        MessageType.SUCCESS
      );
      onHide();
    } catch ({ response }) {
      setLoading(
        t(
          response.data
            ? response.data
            : 'Something went wrong, try that later please'
        )
      );
    }
  };

  const disabledReasonLogInUsingEmail = !email ? t('Fill email') : undefined;

  return (
    <div className={styles.center}>
      <Input
        label={t('Email')}
        name="email"
        type="email"
        autoComplete="email"
        value={email}
        error={formErrors.email}
        onChange={updateForm}
        setFormError={setFormError}
      />
      <Button
        onClick={onSubmit}
        isDisabled={!!disabledReasonLogInUsingEmail}
        disabledReason={disabledReasonLogInUsingEmail}
        isLoading={typeof isLoading === 'string' ? false : isLoading}
        error={typeof isLoading === 'string' ? isLoading : null}
      >
        {t('Reset password')}
      </Button>
    </div>
  );
};

export default ResetPasswordForm;
