import { ReactElement, useEffect, useState } from 'react';
import { useTranslation } from 'utils/localization';
import styles from './ResetPasswordForm.module.scss';
import Button from 'components/basic/Button';
import { axios } from 'utils/axios';
import Input from 'components/form/Input';
import {
  deleteFromRoute,
  MessageType,
  pushMessage,
} from 'components/layout/toast/Toast';
import { validatePassword } from 'utils/browser/validators/validatePassword';
import { useRouter } from 'next/router';

interface Props {
  onHide: () => void;
}

interface Form {
  password: string;
}

const initStateFormErrors: Form = {
  password: null,
};

let token: string;

const SetNewPasswordForm = ({ onHide }: Props): ReactElement => {
  const { t } = useTranslation();
  const Router = useRouter();
  const [isLoading, setLoading] = useState<boolean | string>(false);
  const [form, setForm] = useState<Form>({
    password: '',
  });
  const [formErrors, setFormErrors] = useState<Form>(initStateFormErrors);
  const { password } = form;

  useEffect(() => {
    if (Router.query.token) {
      token = Router.query.token as string;
      deleteFromRoute('token', token);
    }
  }, [Router]);
  const updateForm = (name: string, value: string) => {
    setForm({ ...form, [name]: value });
  };
  const setFormError = (name: string, value: string) => {
    setFormErrors({ ...formErrors, [name]: value });
  };

  const onSubmit = async () => {
    setLoading(true);
    try {
      await axios.post('/auth/set-new-password', {
        password,
        token,
      });
      pushMessage('Password changed.', MessageType.SUCCESS);
      onHide();
    } catch ({ response }) {
      setLoading(
        t(
          response.data
            ? response.data
            : 'Something went wrong, try that later please'
        )
      );
    }
  };

  const disabledReasonLogInUsingEmail = !password
    ? t('Fill password')
    : Object.values(formErrors).find((value) => !!value);

  return (
    <div className={styles.center}>
      <Input
        label={t('New password')}
        name="password"
        type="password"
        autoComplete="password"
        value={password}
        error={formErrors.password}
        validateCb={validatePassword}
        onChange={updateForm}
        setFormError={setFormError}
      />
      <Button
        onClick={onSubmit}
        isDisabled={!!disabledReasonLogInUsingEmail}
        disabledReason={disabledReasonLogInUsingEmail}
        isLoading={typeof isLoading === 'string' ? false : isLoading}
        error={typeof isLoading === 'string' ? isLoading : null}
      >
        {t('Set new password')}
      </Button>
    </div>
  );
};

export default SetNewPasswordForm;
