import { ReactElement } from 'react';
import { useTranslation } from 'utils/localization';
import styles from './Socials.module.scss';
import Button from 'components/basic/Button';
import { getRoute } from 'constants/routes';

const Socials = (): ReactElement => {
  const { t } = useTranslation();

  return (
    <div className={styles.socials}>
      <Button
        href={getRoute('/server_pages/auth/facebook')}
        className={styles.button}
      >
        {t('using facebook')}
      </Button>
      <Button href={getRoute('/server_pages/auth/google')}>
        {t('using google')}
      </Button>
    </div>
  );
};

export default Socials;
