import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import styles from './SignInUpForm.module.scss';
import Button from 'components/basic/Button';
import { axios } from 'utils/axios';
import { validateEmail } from 'utils/browser/validators/validateEmail';
import { validatePassword } from 'utils/browser/validators/validatePassword';
import Input from 'components/form/Input';
import { MessageType, pushMessage } from 'components/layout/toast/Toast';
import { useDispatch } from 'react-redux';
import { reduxUserSet } from '../../../redux/actions/userActions';

interface Props {
  onHide: () => void;
  type: 'SIGN_IN' | 'SIGN_UP';
}

interface Form {
  email: string;
  password: string;
}

const initStateFormErrors: Form = {
  email: null,
  password: null,
};

const SignInUpForm = ({ type, onHide }: Props): ReactElement => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState<boolean | string>(false);
  const [form, setForm] = useState<Form>({
    email: '',
    password: '',
  });
  const [formErrors, setFormErrors] = useState<Form>(initStateFormErrors);
  const { email, password } = form;
  const updateForm = (name: string, value: string) => {
    setForm({ ...form, [name]: value });
  };
  const setFormError = (name: string, value: string) => {
    setFormErrors({ ...formErrors, [name]: value });
  };

  const onSubmit = async () => {
    setLoading(true);
    try {
      const response = await axios.post(
        type === 'SIGN_IN' ? '/auth/login' : '/auth/signup',
        {
          email,
          password,
        }
      );
      if (type === 'SIGN_UP') {
        pushMessage(
          'Verify your email, by visiting sent link.',
          MessageType.SUCCESS
        );
      } else {
        dispatch(reduxUserSet(response.data));
      }
      onHide();
    } catch ({ response }) {
      if (type === 'SIGN_IN' && response.status === 401) {
        setLoading(t('Invalid login data'));
        return;
      }
      setLoading(
        t(
          response.data
            ? response.data
            : 'Something went wrong, try that later please'
        )
      );
    }
  };

  const disabledReasonLogInUsingEmail = !email
    ? t('Fill email')
    : !password
    ? t('Fill password')
    : Object.values(formErrors).find((value) => !!value);

  return (
    <div className={styles.half}>
      <Input
        label={t('Email')}
        name="email"
        type="email"
        autoComplete="email"
        value={email}
        error={formErrors.email}
        validateCb={type === 'SIGN_UP' ? validateEmail : undefined}
        onChange={updateForm}
        setFormError={setFormError}
      />
      <Input
        label={t('Password')}
        name="password"
        type="password"
        autoComplete="password"
        value={password}
        error={formErrors.password}
        validateCb={type === 'SIGN_UP' ? validatePassword : undefined}
        onChange={updateForm}
        setFormError={setFormError}
      />
      <Button
        onClick={onSubmit}
        isDisabled={!!disabledReasonLogInUsingEmail}
        disabledReason={disabledReasonLogInUsingEmail}
        isLoading={typeof isLoading === 'string' ? false : isLoading}
        error={typeof isLoading === 'string' ? isLoading : null}
      >
        {t(type === 'SIGN_UP' ? 'sign up' : 'log in')}
      </Button>
    </div>
  );
};

export default SignInUpForm;
