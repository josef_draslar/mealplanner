import { ReactElement, useEffect, useState } from 'react';
import { AppLayout } from 'components/layout/AppLayout';
import { useTranslation } from 'utils/localization';
import { openSignInUpModal } from 'components/layout/navbar/NavBar';
import { SignInUpModalType } from 'components/layout/modal/full/SignInUpModal';
import { useRouter } from 'next/router';
import { deleteFromRoute } from 'components/layout/toast/Toast';
import { useDispatch, useSelector } from 'react-redux';
import { reduxRestaurantsGet } from 'redux/actions/restaurantActions';
import { ReduxStore } from 'redux/reducers';
import { Restaurant } from 'redux/reducers/restaurantReducer';
import H1 from 'components/typograpfy/H1';
import Button from 'components/basic/Button';
import styles from './HomePage.module.scss';
import { ReduxUser } from 'redux/reducers/userReducer';
import { RestaurantCreation } from 'components/parts/restaurant/RestaurantCreation';
import { axios } from 'utils/axios';
import { OrderCancel } from 'components/parts/order/OrderCancel';
import { OrderReceived } from 'components/parts/order/OrderReceived';
import { OrderInterface } from 'components/pages/order/OrdersPage';
import { OrderNextByOwner } from 'components/parts/order/OrderNextByOwner';
import { formatDate } from 'utils/browser/formatDate';

interface Props {
  restaurantId: number;
}

export const RestaurantOrders = ({ restaurantId }: Props): ReactElement => {
  const { t } = useTranslation();
  const [orders, setOrders] = useState<OrderInterface[]>(null);
  const user: ReduxUser = useSelector((state: ReduxStore) => state.user);
  useEffect(() => {
    fetchOrders();
  }, [user?.token]);

  const fetchOrders = () => {
    axios
      .get(`/restaurants/${restaurantId}/orders`)
      .then(({ data }) => setOrders(data));
  };

  return (
    <div>
      <H1>{t('Orders of restaurant')}</H1>
      {orders ? (
        <div>
          {orders.map((order: OrderInterface, key) => (
            <div key={key}>
              <div>{t('Total price') + ': ' + order.total}</div>
              <div>{t('Status') + ': ' + order.status}</div>
              {!!order.placed && (
                <div>{t('Placed at') + ': ' + formatDate(order.placed)}</div>
              )}
              {!!order.canceled && (
                <div>
                  {t('Canceled at') + ': ' + formatDate(order.canceled)}
                </div>
              )}
              {!!order.processing && (
                <div>
                  {t('Processing at') + ': ' + formatDate(order.processing)}
                </div>
              )}
              {!!order.in_route && (
                <div>
                  {t('In route at') + ': ' + formatDate(order.in_route)}
                </div>
              )}
              {!!order.delivered && (
                <div>
                  {t('Delivered at') + ': ' + formatDate(order.delivered)}
                </div>
              )}
              {!!order.received && (
                <div>
                  {t('Received at') + ': ' + formatDate(order.received)}
                </div>
              )}
              {(order.status === 'PLACED' ||
                order.status === 'PROCESSING' ||
                order.status === 'IN_ROUTE') && (
                <OrderNextByOwner
                  fetch={fetchOrders}
                  orderId={order.id}
                  title={
                    order.status === 'PLACED'
                      ? 'Start processing order'
                      : order.status === 'PROCESSING'
                      ? 'Push order to delivery'
                      : 'Deliver order'
                  }
                  success={
                    order.status === 'PLACED'
                      ? 'Order is processing'
                      : order.status === 'PROCESSING'
                      ? 'Order is delivering'
                      : 'Order is delivered'
                  }
                />
              )}
            </div>
          ))}
        </div>
      ) : (
        <div>loading...</div>
      )}
    </div>
  );
};
