import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import Button from 'components/basic/Button';
import styles from 'components/parts/meal/MealCreationModal.module.scss';
import Modal from 'components/layout/modal/Modal';
import { axios } from 'utils/axios';
import { MessageType, pushMessage } from 'components/layout/toast/Toast';
import { useRouter } from 'next/router';

interface Props {
  mealsToOrder: {
    id: number;
    amount: number;
  }[];
  restaurantId: number;
}

export const OrderCreate = ({
  restaurantId,
  mealsToOrder,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const Router = useRouter();
  const [isModalOpened, setModalOpened] = useState(false);
  const [isLoading, setLoading] = useState<boolean | string>(false);
  const onHide = () => setModalOpened(false);

  const onSubmit = async () => {
    setLoading(true);
    try {
      const response = await axios.post('/orders', {
        meals: mealsToOrder,
        restaurantId,
      });
      pushMessage('Order created', MessageType.SUCCESS);
      console.log(response, response.data);
      await Router.push(`/order?message_key=Created&message_type=SUCCESS`);
    } catch ({ response }) {
      setLoading(t('Something went wrong, try that later please'));
    }
  };

  return (
    <>
      <Button onClick={() => setModalOpened(true)}>{t('Place order')}</Button>
      {isModalOpened && (
        <Modal onHide={onHide} title={t('Place order')}>
          <div className={styles.content}>
            <Button
              onClick={onSubmit}
              isLoading={typeof isLoading === 'string' ? false : isLoading}
              error={typeof isLoading === 'string' ? isLoading : null}
            >
              {t('Place order')}
            </Button>
          </div>
        </Modal>
      )}
    </>
  );
};
