import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import Button from 'components/basic/Button';
import styles from 'components/parts/meal/MealCreationModal.module.scss';
import Modal from 'components/layout/modal/Modal';
import { axios } from 'utils/axios';
import { MessageType, pushMessage } from 'components/layout/toast/Toast';

interface Props {
  fetch: () => void;
  orderId: number;
  title: string;
  success: string;
}

export const OrderNextByOwner = ({
  orderId,
  fetch,
  title,
  success,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const [isModalOpened, setModalOpened] = useState(false);
  const [isLoading, setLoading] = useState<boolean | string>(false);
  const onHide = () => setModalOpened(false);

  const onSubmit = async () => {
    setLoading(true);
    try {
      const response = await axios.post(`/orders/${orderId}/next_state`);
      pushMessage(success, MessageType.SUCCESS);
      fetch();
      onHide();
    } catch ({ response }) {
      setLoading(t('Something went wrong, try that later please'));
    }
  };

  return (
    <>
      <Button onClick={() => setModalOpened(true)}>{t(title)}</Button>
      {isModalOpened && (
        <Modal onHide={onHide} title={t(title)}>
          <div className={styles.content}>
            <Button
              onClick={onSubmit}
              isLoading={typeof isLoading === 'string' ? false : isLoading}
              error={typeof isLoading === 'string' ? isLoading : null}
            >
              {t(title)}
            </Button>
          </div>
        </Modal>
      )}
    </>
  );
};
