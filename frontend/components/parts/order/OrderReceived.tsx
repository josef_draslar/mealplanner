import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import Button from 'components/basic/Button';
import styles from 'components/parts/meal/MealCreationModal.module.scss';
import Modal from 'components/layout/modal/Modal';
import { axios } from 'utils/axios';
import { MessageType, pushMessage } from 'components/layout/toast/Toast';

interface Props {
  fetch: () => void;
  orderId: number;
}

export const OrderReceived = ({ orderId, fetch }: Props): ReactElement => {
  const { t } = useTranslation();
  const [isModalOpened, setModalOpened] = useState(false);
  const [isLoading, setLoading] = useState<boolean | string>(false);
  const onHide = () => setModalOpened(false);

  const onSubmit = async () => {
    setLoading(true);
    try {
      const response = await axios.post(`/orders/${orderId}/received`);
      pushMessage('Order received', MessageType.SUCCESS);
      fetch();
      onHide();
    } catch ({ response }) {
      setLoading(t('Something went wrong, try that later please'));
    }
  };

  return (
    <>
      <Button onClick={() => setModalOpened(true)}>{t('Receive order')}</Button>
      {isModalOpened && (
        <Modal onHide={onHide} title={t('Receive order')}>
          <div className={styles.content}>
            <Button
              onClick={onSubmit}
              isLoading={typeof isLoading === 'string' ? false : isLoading}
              error={typeof isLoading === 'string' ? isLoading : null}
            >
              {t('Receive order')}
            </Button>
          </div>
        </Modal>
      )}
    </>
  );
};
