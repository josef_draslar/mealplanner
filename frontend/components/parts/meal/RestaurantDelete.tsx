import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import Button from 'components/basic/Button';
import MealCreationModal from 'components/parts/meal/MealCreationModal';
import styles from 'components/parts/meal/MealCreationModal.module.scss';
import MealCreationForm from 'components/parts/meal/MealCreationForm';
import Modal from 'components/layout/modal/Modal';
import TextArea from 'components/form/TextArea';
import { axios } from 'utils/axios';
import { MessageType, pushMessage } from 'components/layout/toast/Toast';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { reduxRestaurantsGet } from 'redux/actions/restaurantActions';

interface Props {
  fetchMeals: () => void;
  restaurantId: number;
}

export const RestaurantDelete = ({ restaurantId }: Props): ReactElement => {
  const { t } = useTranslation();
  const Router = useRouter();
  const [isModalOpened, setModalOpened] = useState(false);
  const [isLoading, setLoading] = useState<boolean | string>(false);
  const onHide = () => setModalOpened(false);
  const dispatch = useDispatch();

  const onSubmit = async () => {
    setLoading(true);
    try {
      await axios.delete('/restaurants/' + restaurantId);
      pushMessage('Restaurant deleted.', MessageType.SUCCESS);

      await Router.push(`/?message_key=Deleted&message_type=SUCCESS`);
    } catch ({ response }) {
      setLoading(t('Something went wrong, try that later please'));
    }
  };

  return (
    <>
      <Button onClick={() => setModalOpened(true)}>
        {t('Delete restaurant')}
      </Button>
      {isModalOpened && (
        <Modal onHide={onHide} title={t('Delete restaurant')}>
          <div className={styles.content}>
            <Button
              onClick={onSubmit}
              isLoading={typeof isLoading === 'string' ? false : isLoading}
              error={typeof isLoading === 'string' ? isLoading : null}
            >
              {t('Delete')}
            </Button>
          </div>
        </Modal>
      )}
    </>
  );
};
