import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import Button from 'components/basic/Button';
import MealCreationModal from 'components/parts/meal/MealCreationModal';

interface Props {
  fetchMeals: () => void;
  restaurantId: number;
}

export const MealCreation = ({
  fetchMeals,
  restaurantId,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const [isModalOpened, setModalOpened] = useState(false);

  return (
    <>
      <Button onClick={() => setModalOpened(true)}>{t('Create meal')}</Button>
      {isModalOpened && (
        <MealCreationModal
          fetchMeals={fetchMeals}
          restaurantId={restaurantId}
          onHide={() => setModalOpened(false)}
        />
      )}
    </>
  );
};
