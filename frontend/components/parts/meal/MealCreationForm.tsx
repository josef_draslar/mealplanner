import { ReactElement, useState } from 'react';
import { useTranslation } from 'utils/localization';
import styles from './MealCreationForm.module.scss';
import Button from 'components/basic/Button';
import { axios } from 'utils/axios';
import Input from 'components/form/Input';
import { MessageType, pushMessage } from 'components/layout/toast/Toast';
import { useDispatch } from 'react-redux';
import TextArea from 'components/form/TextArea';
import { Meal } from 'redux/reducers/restaurantReducer';

interface Props {
  onHide: () => void;
  fetchMeals: () => void;
  restaurantId: number;
  meal?: Meal;
}

interface Form {
  name: string;
  description: string;
  price: number;
}

const MealCreationForm = ({
  onHide,
  fetchMeals,
  restaurantId,
  meal,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState<boolean | string>(false);
  const [form, setForm] = useState<Form>({
    name: meal?.name || '',
    description: meal?.description || '',
    price: meal?.price || 10,
  });
  const { name, description, price } = form;
  const updateForm = (name: string, value: string) => {
    setForm({ ...form, [name]: value });
  };

  const onSubmit = async () => {
    setLoading(true);
    if (meal) {
      try {
        await axios.put('/meals/' + meal.id, {
          name,
          description,
          price,
          restaurant_id: restaurantId,
        });
        pushMessage('Meal created.', MessageType.SUCCESS);

        fetchMeals();
        onHide();
      } catch ({ response }) {
        setLoading(t('Something went wrong, try that later please'));
      }
    } else {
      try {
        await axios.post('/meals', {
          name,
          description,
          price,
          restaurant_id: restaurantId,
        });
        pushMessage('Meal created.', MessageType.SUCCESS);

        fetchMeals();
        onHide();
      } catch ({ response }) {
        setLoading(t('Something went wrong, try that later please'));
      }
    }
  };

  const disabledReason = !name
    ? t('Fill name')
    : !description
    ? t('Fill description')
    : !price
    ? t('Fill price')
    : undefined;

  return (
    <div className={styles.half}>
      <Input
        label={t('Name')}
        name="name"
        type="text"
        value={name}
        onChange={updateForm}
      />
      <Input
        label={t('Price')}
        name="price"
        type="number"
        min={1}
        value={price}
        onChange={updateForm}
      />
      <TextArea
        label={t('Description')}
        name="description"
        value={description}
        onChange={updateForm}
      />
      <Button
        onClick={onSubmit}
        isDisabled={!!disabledReason}
        disabledReason={disabledReason}
        isLoading={typeof isLoading === 'string' ? false : isLoading}
        error={typeof isLoading === 'string' ? isLoading : null}
      >
        {t(meal ? 'Save' : 'Create')}
      </Button>
    </div>
  );
};

export default MealCreationForm;
