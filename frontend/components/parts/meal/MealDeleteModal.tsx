import { ReactElement, useState } from 'react';
import Modal from 'components/layout/modal/Modal';
import { useTranslation } from 'utils/localization';
import styles from './MealCreationModal.module.scss';
import Button from 'components/basic/Button';
import { axios } from 'utils/axios';
import { MessageType, pushMessage } from 'components/layout/toast/Toast';

interface Props {
  onHide: () => void;
  fetchMeals: () => void;
  mealId: number;
}

const MealDeleteModal = ({
  onHide,
  fetchMeals,
  mealId,
}: Props): ReactElement => {
  const { t } = useTranslation();
  const [isLoading, setLoading] = useState<boolean | string>(false);

  const onSubmit = async () => {
    setLoading(true);
    try {
      await axios.delete('/meals/' + mealId);
      pushMessage('Meal deleted', MessageType.SUCCESS);

      fetchMeals();
      onHide();
    } catch ({ response }) {
      setLoading(t('Something went wrong, try that later please'));
    }
  };

  return (
    <Modal onHide={onHide} title={t('Delete meal')}>
      <div className={styles.content}>
        <Button
          onClick={onSubmit}
          isLoading={typeof isLoading === 'string' ? false : isLoading}
          error={typeof isLoading === 'string' ? isLoading : null}
        >
          {t('Delete')}
        </Button>
      </div>
    </Modal>
  );
};

export default MealDeleteModal;
