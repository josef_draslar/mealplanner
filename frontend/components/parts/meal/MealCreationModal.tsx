import { ReactElement } from 'react';
import Modal from 'components/layout/modal/Modal';
import { useTranslation } from 'utils/localization';
import styles from './MealCreationModal.module.scss';
import MealCreationForm from 'components/parts/meal/MealCreationForm';
import { Meal } from 'redux/reducers/restaurantReducer';

interface Props {
  onHide: () => void;
  fetchMeals: () => void;
  restaurantId: number;
  meal?: Meal;
}

const MealCreationModal = ({
  onHide,
  fetchMeals,
  restaurantId,
  meal,
}: Props): ReactElement => {
  const { t } = useTranslation();

  return (
    <Modal onHide={onHide} title={t(meal ? 'Edit meal' : 'Create meal')}>
      <div className={styles.content}>
        <MealCreationForm
          fetchMeals={fetchMeals}
          restaurantId={restaurantId}
          onHide={onHide}
          meal={meal}
        />
      </div>
    </Modal>
  );
};

export default MealCreationModal;
