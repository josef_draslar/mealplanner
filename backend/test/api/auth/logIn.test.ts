import { axios } from '../../helpers/axios';
import { ADMIN, USER } from '../../seed/seedTestConstants';

describe('API Auth - logIn', () => {
  const basePath = 'http://localhost:' + process.env.PORT + '/api';

  test('login admin', async () => {
    const post = await axios.post('/auth/login', {
      email: ADMIN.email,
      password: ADMIN.password,
    });
    const token = post.data;

    expect(post.status).toBe(200);
    expect(token).toBeDefined();
  });

  test('login user', async () => {
    const post = await axios.post('/auth/login', {
      email: USER.email,
      password: USER.password,
    });
    const token = post.data;

    expect(post.status).toBe(200);
    expect(token).toBeDefined();
  });

  test('login bad password', async () => {
    try {
      await axios.post('/auth/login', {
        email: USER.email,
        password: USER.password + 'X',
      });
      fail('User logged in');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });

  test('login bad email', async () => {
    try {
      await axios.post('/auth/login', {
        email: USER.email + 'X',
        password: USER.password,
      });
      fail('User logged in');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });

  test('login no password', async () => {
    try {
      await axios.post('/auth/login', {
        email: USER.email,
      });
      fail('User logged in');
    } catch (error) {
      expect(error.response.status).toBe(400);
    }
  });

  test('login no email', async () => {
    try {
      await axios.post('/auth/login', {
        email: USER.email,
      });
      fail('User logged in');
    } catch (error) {
      expect(error.response.status).toBe(400);
    }
  });

  test('login no email and password', async () => {
    try {
      await axios.post('/auth/login', {});
      fail('User logged in');
    } catch (error) {
      expect(error.response.status).toBe(400);
    }
  });

  test('login no data', async () => {
    try {
      await axios.post('/auth/login');
      fail('User logged in');
    } catch (error) {
      expect(error.response.status).toBe(400);
    }
  });
});
