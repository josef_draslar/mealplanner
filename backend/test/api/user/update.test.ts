import { axios } from '../../helpers/axios';
import { USER } from '../../seed/seedTestConstants';
import { INVALID_EMAILS } from '../../contants/emails';
import {
  createUserAndGetId,
  createUserAndGetIdWithToken,
  getUniqueEmail,
  logInAdnGetToken,
  logInAdnGetTokenADMIN,
  logInAdnGetTokenUSER,
} from '../../helpers/userHelper';

describe('API User - update', () => {
  test('update my email', async () => {
    const { id, token } = await createUserAndGetIdWithToken();
    const newEmail = getUniqueEmail();
    const put = await axios.put(
      '/users/' + id,
      {
        email: newEmail,
      },
      {
        headers: { token },
      }
    );
    expect(put.status).toBe(204);

    await logInAdnGetToken(newEmail, USER.password);
  });

  test('update my email with already existing', async () => {
    const { id, token } = await createUserAndGetIdWithToken();

    try {
      await axios.put(
        '/users/' + id,
        {
          email: USER.email,
        },
        {
          headers: { token },
        }
      );
      fail('updated user email to already existing email');
    } catch (error) {
      expect(error.response.status).toBe(409);
    }
  });

  test('update email not signed', async () => {
    const id = await createUserAndGetId();

    try {
      await axios.put(
        '/users/' + id,
        {
          email: getUniqueEmail(),
        },
        {
          headers: {},
        }
      );
      fail('updated user as not signed');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });

  test('update different user email', async () => {
    const id = await createUserAndGetId();

    const token = await logInAdnGetTokenUSER();

    const newEmail = getUniqueEmail();
    try {
      await axios.put(
        '/users/' + id,
        {
          email: newEmail,
        },
        {
          headers: { token },
        }
      );
      fail('regular user updated another user');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });

  test('admin update different user email', async () => {
    const userId = await createUserAndGetId();

    const token = await logInAdnGetTokenADMIN();

    const newEmail = `t${new Date().getTime()}@t.c`;
    const put = await axios.put(
      '/users/' + userId,
      {
        email: newEmail,
      },
      {
        headers: { token },
      }
    );
    expect(put.status).toBe(204);
  });

  test('invalid emails', async () => {
    const { id, token } = await createUserAndGetIdWithToken();

    for (const email of INVALID_EMAILS) {
      try {
        await axios.put(
          '/users/' + id,
          {
            email,
          },
          {
            headers: { token },
          }
        );
        fail('User updated with invalid email ' + email);
      } catch (e) {
        expect(400).toBe(400);
      }
    }
  });
});
