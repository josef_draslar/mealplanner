import { axios } from '../../helpers/axios';
import {
  createUserAndGetId,
  createUserAndGetIdWithToken,
  getUniqueEmail,
  logInAdnGetTokenADMIN,
  logInAdnGetTokenUSER,
} from '../../helpers/userHelper';

//TODO optimize
//TODO cover other API endpoints tests

describe('API User - delete', () => {
  test('delete user as ADMIN', async () => {
    const email = getUniqueEmail();
    const id = await createUserAndGetId(email);
    const token = await logInAdnGetTokenADMIN();
    const res = await axios.delete('/users/' + id, {
      headers: { token },
    });
    expect(res.status).toBe(204);

    try {
      await axios.delete('/users/' + id, {
        headers: { token },
      });
      fail('user is not actually deleted');
    } catch (error) {
      expect(error.response.status).toBe(404);
    }
  });

  test('delete user as USER', async () => {
    const email = getUniqueEmail();
    const id = await createUserAndGetId(email);
    const token = await logInAdnGetTokenUSER();

    try {
      await axios.delete('/users/' + id, {
        headers: { token },
      });
      fail('deleted user as USER');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });

  test('delete user as not signed', async () => {
    const email = getUniqueEmail();
    const id = await createUserAndGetId(email);

    try {
      await axios.delete('/users/' + id, {
        headers: {},
      });
      fail('deleted user as not signed');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });
});
