import { axios } from '../../helpers/axios';
import {
  createUserAndGetId,
  createUserAndGetIdWithToken,
  getUniqueEmail,
  logInAdnGetTokenADMIN,
  logInAdnGetTokenUSER,
} from '../../helpers/userHelper';

describe('API User - getById', () => {
  test('get other user as ADMIN', async () => {
    const email = getUniqueEmail();
    const id = await createUserAndGetId(email);
    const token = await logInAdnGetTokenADMIN();
    const get = await axios.get('/users/' + id, {
      headers: { token },
    });
    expect(get.status).toBe(200);
    expect(get.data).toBeDefined();
    expect(get.data.email).toBe(email);
  });

  test('get other user as ADMIN non existing', async () => {
    const email = getUniqueEmail();
    const id = await createUserAndGetId(email);
    const token = await logInAdnGetTokenADMIN();
    try {
      await axios.get('/users/' + id + 99999, {
        headers: { token },
      });
      fail('got non existing user');
    } catch (error) {
      expect(error.response.status).toBe(404);
    }
  });

  test('get my user details', async () => {
    const { id, token } = await createUserAndGetIdWithToken();
    const get = await axios.get('/users/' + id, {
      headers: { token },
    });
    expect(get.status).toBe(200);
    expect(get.data).toBeDefined();
  });

  test('get other user as USER', async () => {
    const email = getUniqueEmail();
    const id = await createUserAndGetId(email);
    const token = await logInAdnGetTokenUSER();
    try {
      await axios.get('/users/' + id, {
        headers: { token },
      });
      fail('got different user details as normal user');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });

  test('get other user as not signed', async () => {
    const email = getUniqueEmail();
    const id = await createUserAndGetId(email);
    try {
      await axios.get('/users/' + id, {
        headers: {},
      });
      fail('got different user details as normal user');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });
});
