"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("../../helpers/axios");
var seedTestConstants_1 = require("../../seed/seedTestConstants");
var failWithError_1 = require("../../helpers/failWithError");
var emails_1 = require("../../contants/emails");
var PASSWORD = 'Aa12345!';
var INVALID_PASSWORDS = [
    'Aa12345!á',
    'Aa1234!',
    'A123456!',
    'a123456!',
    '1234567!',
    'Aa123456',
];
var VALID_EMAILS = [
    'email@example.com',
    'firstname.lastname@example.com',
    'email@subdomain.example.com',
    'firstname+lastname@example.com',
    '1234567890@example.com',
    'email@example-one.com',
    'email@example.name',
    'email@example.museum',
    'email@example.co.jp',
    'firstname-lastname@example.com',
    'email@example.web',
    'email@111.222.333.44444',
];
describe('API User - sign up', function () {
    var basePath = 'http://localhost:' + process.env.PORT + '/api';
    test('success and login', function () { return __awaiter(void 0, void 0, void 0, function () {
        var email, password, post, post2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    email = "t" + new Date().getTime() + "@t.c";
                    password = PASSWORD;
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup', {
                            email: email,
                            password: password,
                        })];
                case 1:
                    post = _a.sent();
                    expect(post.data.password).not.toBeDefined();
                    expect(post.status).toBe(200);
                    return [4 /*yield*/, axios_1.axios.post('/auth/login', {
                            email: email,
                            password: password,
                        })];
                case 2:
                    post2 = _a.sent();
                    expect(post2.status).toBe(200);
                    expect(post2.data).toBeDefined();
                    return [2 /*return*/];
            }
        });
    }); });
    test('existing email', function () { return __awaiter(void 0, void 0, void 0, function () {
        var e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup', {
                            email: seedTestConstants_1.ADMIN.email,
                            password: PASSWORD,
                        })];
                case 1:
                    _a.sent();
                    return [3 /*break*/, 3];
                case 2:
                    e_1 = _a.sent();
                    expect(e_1.response.status).toBe(409);
                    return [2 /*return*/];
                case 3:
                    fail('User created with already existing email');
                    return [2 /*return*/];
            }
        });
    }); });
    test('missing password', function () { return __awaiter(void 0, void 0, void 0, function () {
        var e_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup', {
                            email: seedTestConstants_1.USER.email,
                        })];
                case 1:
                    _a.sent();
                    fail('User created with already existing email');
                    return [3 /*break*/, 3];
                case 2:
                    e_2 = _a.sent();
                    expect(400).toBe(400);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    test('missing email', function () { return __awaiter(void 0, void 0, void 0, function () {
        var e_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup', {
                            password: PASSWORD,
                        })];
                case 1:
                    _a.sent();
                    fail('User created with already existing email');
                    return [3 /*break*/, 3];
                case 2:
                    e_3 = _a.sent();
                    expect(400).toBe(400);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    test('missing email and password', function () { return __awaiter(void 0, void 0, void 0, function () {
        var e_4;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup', {})];
                case 1:
                    _a.sent();
                    fail('User created with already existing email');
                    return [3 /*break*/, 3];
                case 2:
                    e_4 = _a.sent();
                    expect(400).toBe(400);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    test('missing data', function () { return __awaiter(void 0, void 0, void 0, function () {
        var e_5;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup')];
                case 1:
                    _a.sent();
                    fail('User created with already existing email');
                    return [3 /*break*/, 3];
                case 2:
                    e_5 = _a.sent();
                    expect(400).toBe(400);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    test('invalid email', function () { return __awaiter(void 0, void 0, void 0, function () {
        var e_6;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup', {
                            email: 'asde',
                            password: PASSWORD,
                        })];
                case 1:
                    _a.sent();
                    fail('User created with invalid email');
                    return [3 /*break*/, 3];
                case 2:
                    e_6 = _a.sent();
                    expect(400).toBe(400);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    test('invalid passwords', function () { return __awaiter(void 0, void 0, void 0, function () {
        var email, _i, INVALID_PASSWORDS_1, password, e_7;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    email = "t" + new Date().getTime() + "@t.c";
                    _i = 0, INVALID_PASSWORDS_1 = INVALID_PASSWORDS;
                    _a.label = 1;
                case 1:
                    if (!(_i < INVALID_PASSWORDS_1.length)) return [3 /*break*/, 6];
                    password = INVALID_PASSWORDS_1[_i];
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 4, , 5]);
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup', {
                            email: email,
                            password: password,
                        })];
                case 3:
                    _a.sent();
                    fail('User created with invalid password ' + password);
                    return [3 /*break*/, 5];
                case 4:
                    e_7 = _a.sent();
                    expect(400).toBe(400);
                    return [3 /*break*/, 5];
                case 5:
                    _i++;
                    return [3 /*break*/, 1];
                case 6: return [2 /*return*/];
            }
        });
    }); });
    test('invalid emails', function () { return __awaiter(void 0, void 0, void 0, function () {
        var password, _i, INVALID_EMAILS_1, email, e_8;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    password = PASSWORD;
                    _i = 0, INVALID_EMAILS_1 = emails_1.INVALID_EMAILS;
                    _a.label = 1;
                case 1:
                    if (!(_i < INVALID_EMAILS_1.length)) return [3 /*break*/, 6];
                    email = INVALID_EMAILS_1[_i];
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 4, , 5]);
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup', {
                            email: email,
                            password: password,
                        })];
                case 3:
                    _a.sent();
                    fail('User created with invalid email ' + email);
                    return [3 /*break*/, 5];
                case 4:
                    e_8 = _a.sent();
                    expect(400).toBe(400);
                    return [3 /*break*/, 5];
                case 5:
                    _i++;
                    return [3 /*break*/, 1];
                case 6: return [2 /*return*/];
            }
        });
    }); });
    test('valid emails', function () { return __awaiter(void 0, void 0, void 0, function () {
        var password, _i, VALID_EMAILS_1, email, post, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    password = PASSWORD;
                    _i = 0, VALID_EMAILS_1 = VALID_EMAILS;
                    _a.label = 1;
                case 1:
                    if (!(_i < VALID_EMAILS_1.length)) return [3 /*break*/, 7];
                    email = VALID_EMAILS_1[_i];
                    post = void 0;
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 4, , 5]);
                    return [4 /*yield*/, axios_1.axios.post('/auth/signup', {
                            email: email,
                            password: password,
                        })];
                case 3:
                    post = _a.sent();
                    return [3 /*break*/, 5];
                case 4:
                    error_1 = _a.sent();
                    failWithError_1.failWithError(error_1, { email: email });
                    return [2 /*return*/];
                case 5:
                    expect(post.data.id).toBeDefined();
                    expect(post.data.password).not.toBeDefined();
                    expect(post.status).toBe(200);
                    _a.label = 6;
                case 6:
                    _i++;
                    return [3 /*break*/, 1];
                case 7: return [2 /*return*/];
            }
        });
    }); });
});
