import { axios } from '../../helpers/axios';
import { ADMIN, USER } from '../../seed/seedTestConstants';
import { failWithError } from '../../helpers/failWithError';
import { INVALID_EMAILS } from '../../contants/emails';

const PASSWORD = 'Aa12345!';
const INVALID_PASSWORDS = [
  'Aa12345!á',
  'Aa1234!',
  'A123456!',
  'a123456!',
  '1234567!',
  'Aa123456',
];
const VALID_EMAILS = [
  'email@example.com',
  'firstname.lastname@example.com',
  'email@subdomain.example.com',
  'firstname+lastname@example.com',
  '1234567890@example.com',
  'email@example-one.com',
  'email@example.name',
  'email@example.museum',
  'email@example.co.jp',
  'firstname-lastname@example.com',
  'email@example.web',
  'email@111.222.333.44444',
];

describe('API User - sign up', () => {
  const basePath = 'http://localhost:' + process.env.PORT + '/api';

  test('success and login', async () => {
    const email = `t${new Date().getTime()}@t.c`;
    const password = PASSWORD;
    const post = await axios.post('/auth/signup', {
      email,
      password,
    });

    expect(post.data.password).not.toBeDefined();
    expect(post.status).toBe(200);

    const post2 = await axios.post('/auth/login', {
      email,
      password,
    });
    expect(post2.status).toBe(200);
    expect(post2.data).toBeDefined();
  });

  test('existing email', async () => {
    try {
      await axios.post('/auth/signup', {
        email: ADMIN.email,
        password: PASSWORD,
      });
    } catch (e) {
      expect(e.response.status).toBe(409);
      return;
    }
    fail('User created with already existing email');
  });

  test('missing password', async () => {
    try {
      await axios.post('/auth/signup', {
        email: USER.email,
      });
      fail('User created with already existing email');
    } catch (e) {
      expect(400).toBe(400);
    }
  });

  test('missing email', async () => {
    try {
      await axios.post('/auth/signup', {
        password: PASSWORD,
      });
      fail('User created with already existing email');
    } catch (e) {
      expect(400).toBe(400);
    }
  });

  test('missing email and password', async () => {
    try {
      await axios.post('/auth/signup', {});
      fail('User created with already existing email');
    } catch (e) {
      expect(400).toBe(400);
    }
  });

  test('missing data', async () => {
    try {
      await axios.post('/auth/signup');
      fail('User created with already existing email');
    } catch (e) {
      expect(400).toBe(400);
    }
  });

  test('invalid email', async () => {
    try {
      await axios.post('/auth/signup', {
        email: 'asde',
        password: PASSWORD,
      });
      fail('User created with invalid email');
    } catch (e) {
      expect(400).toBe(400);
    }
  });

  test('invalid passwords', async () => {
    const email = `t${new Date().getTime()}@t.c`;
    for (const password of INVALID_PASSWORDS) {
      try {
        await axios.post('/auth/signup', {
          email,
          password,
        });
        fail('User created with invalid password ' + password);
      } catch (e) {
        expect(400).toBe(400);
      }
    }
  });

  test('invalid emails', async () => {
    const password = PASSWORD;
    for (const email of INVALID_EMAILS) {
      try {
        await axios.post('/auth/signup', {
          email,
          password,
        });
        fail('User created with invalid email ' + email);
      } catch (e) {
        expect(400).toBe(400);
      }
    }
  });

  test('valid emails', async () => {
    const password = PASSWORD;
    for (const email of VALID_EMAILS) {
      let post;
      try {
        post = await axios.post('/auth/signup', {
          email,
          password,
        });
      } catch (error) {
        failWithError(error, { email });
        return;
      }

      expect(post.data.id).toBeDefined();
      expect(post.data.password).not.toBeDefined();
      expect(post.status).toBe(200);
    }
  });
});
