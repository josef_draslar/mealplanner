"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("../../helpers/axios");
var seedTestConstants_1 = require("../../seed/seedTestConstants");
var emails_1 = require("../../contants/emails");
var userHelper_1 = require("../../helpers/userHelper");
describe('API User - update', function () {
    test('update my email', function () { return __awaiter(void 0, void 0, void 0, function () {
        var _a, id, token, newEmail, put;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, userHelper_1.createUserAndGetIdWithToken()];
                case 1:
                    _a = _b.sent(), id = _a.id, token = _a.token;
                    newEmail = userHelper_1.getUniqueEmail();
                    return [4 /*yield*/, axios_1.axios.put('/users/' + id, {
                            email: newEmail,
                        }, {
                            headers: { token: token },
                        })];
                case 2:
                    put = _b.sent();
                    expect(put.status).toBe(204);
                    return [4 /*yield*/, userHelper_1.logInAdnGetToken(newEmail, seedTestConstants_1.USER.password)];
                case 3:
                    _b.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    test('update my email with already existing', function () { return __awaiter(void 0, void 0, void 0, function () {
        var _a, id, token, error_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, userHelper_1.createUserAndGetIdWithToken()];
                case 1:
                    _a = _b.sent(), id = _a.id, token = _a.token;
                    _b.label = 2;
                case 2:
                    _b.trys.push([2, 4, , 5]);
                    return [4 /*yield*/, axios_1.axios.put('/users/' + id, {
                            email: seedTestConstants_1.USER.email,
                        }, {
                            headers: { token: token },
                        })];
                case 3:
                    _b.sent();
                    fail('updated user email to already existing email');
                    return [3 /*break*/, 5];
                case 4:
                    error_1 = _b.sent();
                    expect(error_1.response.status).toBe(409);
                    return [3 /*break*/, 5];
                case 5: return [2 /*return*/];
            }
        });
    }); });
    test('update email not signed', function () { return __awaiter(void 0, void 0, void 0, function () {
        var id, error_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, userHelper_1.createUserAndGetId()];
                case 1:
                    id = _a.sent();
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 4, , 5]);
                    return [4 /*yield*/, axios_1.axios.put('/users/' + id, {
                            email: userHelper_1.getUniqueEmail(),
                        }, {
                            headers: {},
                        })];
                case 3:
                    _a.sent();
                    fail('updated user as not signed');
                    return [3 /*break*/, 5];
                case 4:
                    error_2 = _a.sent();
                    expect(error_2.response.status).toBe(401);
                    return [3 /*break*/, 5];
                case 5: return [2 /*return*/];
            }
        });
    }); });
    test('update different user email', function () { return __awaiter(void 0, void 0, void 0, function () {
        var id, token, newEmail, error_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, userHelper_1.createUserAndGetId()];
                case 1:
                    id = _a.sent();
                    return [4 /*yield*/, userHelper_1.logInAdnGetTokenUSER()];
                case 2:
                    token = _a.sent();
                    newEmail = userHelper_1.getUniqueEmail();
                    _a.label = 3;
                case 3:
                    _a.trys.push([3, 5, , 6]);
                    return [4 /*yield*/, axios_1.axios.put('/users/' + id, {
                            email: newEmail,
                        }, {
                            headers: { token: token },
                        })];
                case 4:
                    _a.sent();
                    fail('regular user updated another user');
                    return [3 /*break*/, 6];
                case 5:
                    error_3 = _a.sent();
                    expect(error_3.response.status).toBe(401);
                    return [3 /*break*/, 6];
                case 6: return [2 /*return*/];
            }
        });
    }); });
    test('admin update different user email', function () { return __awaiter(void 0, void 0, void 0, function () {
        var userId, token, newEmail, put;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, userHelper_1.createUserAndGetId()];
                case 1:
                    userId = _a.sent();
                    return [4 /*yield*/, userHelper_1.logInAdnGetTokenADMIN()];
                case 2:
                    token = _a.sent();
                    newEmail = "t" + new Date().getTime() + "@t.c";
                    return [4 /*yield*/, axios_1.axios.put('/users/' + userId, {
                            email: newEmail,
                        }, {
                            headers: { token: token },
                        })];
                case 3:
                    put = _a.sent();
                    expect(put.status).toBe(204);
                    return [2 /*return*/];
            }
        });
    }); });
    test('invalid emails', function () { return __awaiter(void 0, void 0, void 0, function () {
        var _a, id, token, _i, INVALID_EMAILS_1, email, e_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, userHelper_1.createUserAndGetIdWithToken()];
                case 1:
                    _a = _b.sent(), id = _a.id, token = _a.token;
                    _i = 0, INVALID_EMAILS_1 = emails_1.INVALID_EMAILS;
                    _b.label = 2;
                case 2:
                    if (!(_i < INVALID_EMAILS_1.length)) return [3 /*break*/, 7];
                    email = INVALID_EMAILS_1[_i];
                    _b.label = 3;
                case 3:
                    _b.trys.push([3, 5, , 6]);
                    return [4 /*yield*/, axios_1.axios.put('/users/' + id, {
                            email: email,
                        }, {
                            headers: { token: token },
                        })];
                case 4:
                    _b.sent();
                    fail('User updated with invalid email ' + email);
                    return [3 /*break*/, 6];
                case 5:
                    e_1 = _b.sent();
                    expect(400).toBe(400);
                    return [3 /*break*/, 6];
                case 6:
                    _i++;
                    return [3 /*break*/, 2];
                case 7: return [2 /*return*/];
            }
        });
    }); });
});
