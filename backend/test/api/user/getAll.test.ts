import { axios } from '../../helpers/axios';
import {
  logInAdnGetTokenADMIN,
  logInAdnGetTokenUSER,
} from '../../helpers/userHelper';

describe('API User - getAll', () => {
  test('get as admin', async () => {
    const token = await logInAdnGetTokenADMIN();
    const get = await axios.get('/users', {
      headers: { token },
    });
    expect(get.status).toBe(200);
    expect(get.data).toBeDefined();
    expect(get.data.length).not.toBe(0);
  });

  test('get as user', async () => {
    const token = await logInAdnGetTokenUSER();
    try {
      await axios.get('/users', {
        headers: { token },
      });
      fail('queried all users and normal user');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });

  test('get as not signed', async () => {
    try {
      await axios.get('/users', {
        headers: {},
      });
      fail('queried all users and not signed');
    } catch (error) {
      expect(error.response.status).toBe(401);
    }
  });
});
