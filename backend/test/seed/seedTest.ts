import { Connection } from 'typeorm/connection/Connection';
import { RoleEnum, User } from '../../src/entities/User';
import { ADMIN, USER } from './seedTestConstants';

export const seedTestDatabase = async (connection: Connection) => {
  const userRepository = connection.getRepository(User);

  let user = new User();
  user.email = ADMIN.email;
  user.password = ADMIN.password;
  user.role = ADMIN.role as RoleEnum;
  user.hashPassword();
  await userRepository.save(user);

  user = new User();
  user.email = USER.email;
  user.password = USER.password;
  user.role = RoleEnum.USER;
  user.hashPassword();
  await userRepository.save(user);
  console.log('user created');
};
