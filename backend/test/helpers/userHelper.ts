import { ADMIN, USER } from '../seed/seedTestConstants';
import { axios } from './axios';

export const getUniqueEmail = (): string => `t${new Date().getTime()}@t.c`;

export const createUserAndGetId = async (
  email: string = getUniqueEmail()
): Promise<number> => {
  const password = USER.password;
  const post = await axios.post('/auth/signup', {
    email,
    password,
  });

  expect(post.status).toBe(200);

  return post.data.id;
};

export const createUserAndGetIdWithToken = async (): Promise<{
  id: number;
  token: string;
}> => {
  const email = getUniqueEmail();
  const id = await createUserAndGetId(email);
  const token = await logInAdnGetToken(email, USER.password);

  return { id, token };
};

export const logInAdnGetToken = async (
  email: string,
  password: string
): Promise<string> => {
  const postLogIn = await axios.post('/auth/login', {
    email,
    password,
  });

  expect(postLogIn.status).toBe(200);

  return postLogIn.data;
};

export const logInAdnGetTokenADMIN = async (): Promise<string> =>
  logInAdnGetToken(ADMIN.email, ADMIN.password);

export const logInAdnGetTokenUSER = async (): Promise<string> =>
  logInAdnGetToken(USER.email, USER.password);
