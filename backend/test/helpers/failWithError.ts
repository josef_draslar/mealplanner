import { AxiosError } from 'axios';

export const failWithError = (
  error: AxiosError,
  params: { [key: string]: string }
) =>
  fail(
    'Status code: ' +
      error.response.status +
      '; ' +
      error.response.data +
      (params
        ? '; ' +
          Object.keys(params)
            .map((key) => `${key}='${params[key]}'`)
            .join(', ')
        : '')
  );
