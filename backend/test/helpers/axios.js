"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.axios = void 0;
var axios_1 = require("axios");
var axiosConfig = {
    baseURL: 'http://localhost:' + process.env.PORT + '/api',
    timeout: 5000,
};
exports.axios = axios_1.default.create(axiosConfig);
