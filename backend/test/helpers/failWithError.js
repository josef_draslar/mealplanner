"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.failWithError = void 0;
var failWithError = function (error, params) {
    return fail('Status code: ' +
        error.response.status +
        '; ' +
        error.response.data +
        (params
            ? '; ' +
                Object.keys(params)
                    .map(function (key) { return key + "='" + params[key] + "'"; })
                    .join(', ')
            : ''));
};
exports.failWithError = failWithError;
