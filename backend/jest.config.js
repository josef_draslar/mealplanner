require('dotenv').config()

module.exports = {
  setupFilesAfterEnv: ['./test/jest.setup.js'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  "roots": [
      "test"
  ],
  "testMatch": [
    "**/*.test.ts"
  ],
  "transform": {
    "^.+\\.(ts|tsx)$": "ts-jest"
  },
};