import { Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { RoleEnum, User } from '../entities/User';
import { RequestWithContext } from './setUserContext';

const checkRole = async (
  req: RequestWithContext,
  res: Response,
  next: NextFunction,
  roles?: RoleEnum[]
) => {
  if (!req.context || !req.context.user) {
    res.status(401).send();
    return;
  }
  if (!roles) {
    next();
    return;
  }
  const id = req.context.user.id;

  const userRepository = getRepository(User);
  let user: User;
  try {
    user = await userRepository.findOneOrFail(id);
  } catch (id) {
    res.status(401).send();
  }

  if (roles.indexOf(user.role) > -1) next();
  else res.status(401).send();
};

export const checkSignedIn = async (
  req: RequestWithContext,
  res: Response,
  next: NextFunction
) => {
  return await checkRole(req, res, next);
};

export const checkSignedInOwner = async (
  req: RequestWithContext,
  res: Response,
  next: NextFunction
) => {
  return await checkRole(req, res, next, [RoleEnum.OWNER, RoleEnum.ADMIN]);
};

export const checkSignedInAdmin = async (
  req: RequestWithContext,
  res: Response,
  next: NextFunction
) => {
  return await checkRole(req, res, next, [RoleEnum.ADMIN]);
};
