import { Express } from 'express';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import { setUserContext } from './setUserContext';
import morgan from 'morgan';
import './passport';

export const setUpMiddlewares = (app: Express) => {
  app.use(cors());
  app.use(helmet());
  app.use(bodyParser.json());
  app.use(morgan('combined'));
  app.use(setUserContext);
};
