import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import config from '../config/config';
import { User } from '../entities/User';

export interface RequestWithContext extends Request {
  context: {
    user?: User;
  };
}

export const setUserContext = (
  req: RequestWithContext,
  res: Response,
  next: NextFunction
) => {
  const token = <string>req.headers['token'];
  let jwtPayload;
  try {
    jwtPayload = <any>jwt.verify(token, config.jwtSecret);
  } catch (error) {
    next();
    return;
  }

  const { userId, email, role } = jwtPayload;
  const newToken = jwt.sign({ userId, email, role }, config.jwtSecret, {
    expiresIn: '1h',
  });
  res.setHeader('token', newToken);
  const user = new User();
  user.id = userId;
  user.email = email;
  user.role = role;
  req.context = {
    user,
  };

  next();
};
