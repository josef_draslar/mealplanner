import passport, { Profile } from 'passport';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import UserController from '../controllers/UserController';
import { getToken } from '../utils/getToken';

const verify = async (
  accessToken: string,
  refreshToken: string,
  profile: Profile,
  done: (error: any, user?: any, info?: any) => void
) => {
  try {
    const user = await UserController.getOrCreateUserServer(
      accessToken,
      refreshToken,
      profile.emails[0].value
    );
    const token = getToken(user);

    done(null, token);
  } catch (err) {
    return done(err);
  }
};

passport.use(
  new FacebookStrategy(
    {
      clientID: process.env.FACEBOOK_APP_ID,
      clientSecret: process.env.FACEBOOK_APP_SECRET,
      callbackURL: process.env.DOMAIN + '/server_pages/auth/facebook/callback',
      profileFields: ['id', 'emails', 'name'],
    },
    verify
  )
);

const verifyGoogle = async (
  accessToken: string,
  refreshToken: string,
  profile: Profile,
  done: (error: any, user?: any, info?: any) => void
) => {
  try {
    const user = await UserController.getOrCreateUserServer(
      accessToken,
      refreshToken,
      profile.emails[0].value,
      profile.id
    );
    const token = getToken(user);

    done(null, token);
  } catch (err) {
    return done(err);
  }
};

// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_APP_ID,
      clientSecret: process.env.GOOGLE_APP_SECRET,
      callbackURL: process.env.DOMAIN + '/server_pages/auth/google/callback',
    },
    verifyGoogle
  )
);
