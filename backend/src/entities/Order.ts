import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Meal } from './Meal';
import { User } from './User';
import { Restaurant } from './Restaurant';
import { MealOrder } from './MealOrder';

export enum OrderStateEnum {
  'PLACED' = 'PLACED',
  'CANCELED' = 'CANCELED',
  'PROCESSING' = 'PROCESSING',
  'IN_ROUTE' = 'IN_ROUTE',
  'DELIVERED' = 'DELIVERED',
  'RECEIVED' = 'RECEIVED',
}

@Entity()
export class Order {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column('int')
  total: number;

  @Column('enum', {
    name: 'status',
    enum: OrderStateEnum,
  })
  status: OrderStateEnum;

  @Column('varchar', { nullable: true })
  placed?: Date;

  @Column('varchar', { nullable: true })
  canceled?: Date;

  @Column('varchar', { nullable: true })
  processing?: Date;

  @Column('varchar', { nullable: true })
  in_route?: Date;

  @Column('varchar', { nullable: true })
  delivered?: Date;

  @Column('varchar', { nullable: true })
  received?: Date;

  @Column()
  restaurantId: number;

  @Column()
  userId: number;

  @ManyToOne(() => User, (user: User) => user.restaurants)
  user: User;

  @ManyToOne(() => Restaurant, (restaurant: Restaurant) => restaurant.orders)
  restaurant: Restaurant;

  @OneToMany(() => MealOrder, (mealOrders: MealOrder) => mealOrders.order)
  mealOrders!: MealOrder[];
}
