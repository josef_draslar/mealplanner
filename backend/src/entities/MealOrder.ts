import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Meal } from './Meal';
import { Order } from './Order';

@Entity()
export class MealOrder {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column()
  amount: number;

  @Column()
  public mealId!: number;

  @Column()
  public orderId!: number;

  @ManyToOne(() => Order, (order: Order) => order.mealOrders)
  public order!: Order;

  @ManyToOne(() => Meal, (meal: Meal) => meal.mealOrders)
  public meal!: Meal;
}
