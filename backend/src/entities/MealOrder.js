"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MealPlan = void 0;
var typeorm_1 = require("typeorm");
var User_1 = require("./User");
var Meal_1 = require("./Meal");
var MealOrder = /** @class */ (function () {
    function MealPlan() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: 'int' })
    ], MealPlan.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column('int', { name: 'order', nullable: true })
    ], MealPlan.prototype, "order", void 0);
    __decorate([
        typeorm_1.Column('int', { name: 'top_order', nullable: true })
    ], MealPlan.prototype, "topOrder", void 0);
    __decorate([
        typeorm_1.Column()
    ], MealPlan.prototype, "mealId", void 0);
    __decorate([
        typeorm_1.Column()
    ], MealPlan.prototype, "userId", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return User_1.User; }, function (user) { return user.mealPlans; })
    ], MealPlan.prototype, "user", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Meal_1.Meal; }, function (meal) { return meal.mealPlans; })
    ], MealPlan.prototype, "meal", void 0);
    MealPlan = __decorate([
        typeorm_1.Index('idx_plan__user', ['user'], {}),
        typeorm_1.Entity('plan', { schema: 'mealplanner' })
    ], MealPlan);
    return MealPlan;
}());
exports.MealPlan = MealOrder;
