import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm/index';
import * as bcrypt from 'bcryptjs';
import { MealOrder } from './MealOrder';
import { Restaurant } from './Restaurant';
import { Order } from './Order';

export enum RoleEnum {
  'USER' = 'USER',
  'OWNER' = 'OWNER',
  'ADMIN' = 'ADMIN',
}

@Entity()
export class User {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column('varchar', { unique: true, length: 89 })
  email: string;

  @Column('varchar', { nullable: true, length: 100 })
  password?: string;

  @Column('boolean', { default: false })
  isVerified: boolean;

  @Column('boolean', { default: false })
  blocked: boolean;

  @Column('varchar', { nullable: true })
  emailToken?: string;

  @Column('varchar', { nullable: true })
  accessToken?: string;

  @Column('varchar', { nullable: true })
  refreshToken?: string;

  @Column('varchar', { nullable: true })
  googleId?: string;

  @Column('enum', { enum: RoleEnum })
  role: RoleEnum;

  @OneToMany(() => Order, (orders: Order) => orders.user)
  orders!: Order[];

  @OneToMany(() => Restaurant, (restaurants: Restaurant) => restaurants.user)
  restaurants!: Restaurant[];

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  hashPassword() {
    this.password = bcrypt.hashSync(this.password, 8);
  }

  checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
    return bcrypt.compareSync(unencryptedPassword, this.password);
  }
}
