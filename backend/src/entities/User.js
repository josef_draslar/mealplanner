"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = exports.RoleEnum = void 0;
var typeorm_1 = require("typeorm");
var ShoppingListItem_1 = require("./ShoppingListItem");
var index_1 = require("typeorm/index");
var bcrypt = require("bcryptjs");
var MealPlan_1 = require("./MealPlan");
var Meal_1 = require("./Meal");
var RoleEnum;
(function (RoleEnum) {
    RoleEnum["USER"] = "USER";
    RoleEnum["ADMIN"] = "ADMIN";
})(RoleEnum = exports.RoleEnum || (exports.RoleEnum = {}));
var User = /** @class */ (function () {
    function User() {
    }
    User.prototype.hashPassword = function () {
        this.password = bcrypt.hashSync(this.password, 8);
    };
    User.prototype.checkIfUnencryptedPasswordIsValid = function (unencryptedPassword) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    };
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: 'int' })
    ], User.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column('varchar', { unique: true, length: 89 })
    ], User.prototype, "email", void 0);
    __decorate([
        typeorm_1.Column('varchar', { length: 100 })
    ], User.prototype, "password", void 0);
    __decorate([
        typeorm_1.Column('enum', { enum: RoleEnum })
    ], User.prototype, "role", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return MealPlan_1.MealPlan; }, function (mealPlans) { return mealPlans.user; })
    ], User.prototype, "mealPlans", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return ShoppingListItem_1.ShoppingListItem; }, function (shoppingList) { return shoppingList.user; })
    ], User.prototype, "shoppingListItems", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Meal_1.Meal; }, function (meal) { return meal.user; })
    ], User.prototype, "meals", void 0);
    __decorate([
        typeorm_1.Column(),
        index_1.CreateDateColumn()
    ], User.prototype, "createdAt", void 0);
    __decorate([
        typeorm_1.Column(),
        index_1.UpdateDateColumn()
    ], User.prototype, "updatedAt", void 0);
    User = __decorate([
        typeorm_1.Entity()
    ], User);
    return User;
}());
exports.User = User;
