import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm/index';
import { MealOrder } from './MealOrder';
import { Restaurant } from './Restaurant';
import { Order } from './Order';

@Entity()
export class Meal {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column('varchar', { length: 60 })
  name: string;

  @Column('varchar', { length: 255 })
  description: string;

  @Column('int')
  price: number;

  @Column('boolean', { default: false })
  deprecated: boolean;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  @Column()
  public restaurantId!: number;

  @ManyToOne(() => Restaurant, (restaurant: Restaurant) => restaurant.meals)
  restaurant: Restaurant;

  @OneToMany(() => MealOrder, (mealPlans: MealOrder) => mealPlans.meal)
  mealOrders!: MealOrder[];
}
