import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm/index';
import { Meal } from './Meal';
import { User } from './User';
import { Order } from './Order';

@Entity()
export class Restaurant {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column('varchar', { length: 60 })
  name: string;

  @Column('varchar', { length: 255 })
  description: string;

  @Column('boolean', { default: false })
  deprecated: boolean;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  @Column()
  public userId!: number;

  @ManyToOne(() => User, (user: User) => user.restaurants)
  user: User;

  @OneToMany(() => Order, (orders: Order) => orders.restaurant)
  orders!: Order[];

  @OneToMany(() => Meal, (meals: Meal) => meals.restaurant)
  meals!: Meal[];
}
