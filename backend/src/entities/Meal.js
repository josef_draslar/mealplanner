"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Meal = exports.BreakfastLunchDinnerEnum = void 0;
var typeorm_1 = require("typeorm");
var User_1 = require("./User");
var MealToIngredient_1 = require("./MealToIngredient");
var MealPlan_1 = require("./MealPlan");
var index_1 = require("typeorm/index");
var BreakfastLunchDinnerEnum;
(function (BreakfastLunchDinnerEnum) {
    BreakfastLunchDinnerEnum["BREAKFAST"] = "BREAKFAST";
    BreakfastLunchDinnerEnum["LUNCH"] = "LUNCH";
    BreakfastLunchDinnerEnum["DINNER"] = "DINNER";
    BreakfastLunchDinnerEnum["SNACK"] = "SNACK";
})(BreakfastLunchDinnerEnum = exports.BreakfastLunchDinnerEnum || (exports.BreakfastLunchDinnerEnum = {}));
var Meal = /** @class */ (function () {
    function Meal() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: 'int' })
    ], Meal.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column('varchar', { nullable: true, unique: true, length: 60 })
    ], Meal.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column('varchar', { length: 255 })
    ], Meal.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column('longtext')
    ], Meal.prototype, "steps", void 0);
    __decorate([
        typeorm_1.Column('varchar', { name: 'original_recipe_url', length: 255 })
    ], Meal.prototype, "originalRecipeUrl", void 0);
    __decorate([
        typeorm_1.Column('varchar', { name: 'kaloricke_tabulky_url', length: 255 })
    ], Meal.prototype, "kalorickeTabulkyUrl", void 0);
    __decorate([
        typeorm_1.Column('varchar', { name: 'image_url', length: 255 })
    ], Meal.prototype, "imageUrl", void 0);
    __decorate([
        typeorm_1.Column(),
        index_1.CreateDateColumn()
    ], Meal.prototype, "createdAt", void 0);
    __decorate([
        typeorm_1.Column(),
        index_1.UpdateDateColumn()
    ], Meal.prototype, "updatedAt", void 0);
    __decorate([
        typeorm_1.Column('enum', {
            name: 'breakfast_lunch_dinner_type',
            enum: BreakfastLunchDinnerEnum,
        })
    ], Meal.prototype, "breakfastLunchDinnerType", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return User_1.User; }, function (user) { return user.meals; })
    ], Meal.prototype, "user", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return MealToIngredient_1.MealToIngredient; }, function (ingredients) { return ingredients.meal; })
    ], Meal.prototype, "ingredients", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return MealPlan_1.MealPlan; }, function (mealPlans) { return mealPlans.meal; })
    ], Meal.prototype, "mealPlans", void 0);
    Meal = __decorate([
        typeorm_1.Entity()
    ], Meal);
    return Meal;
}());
exports.Meal = Meal;
