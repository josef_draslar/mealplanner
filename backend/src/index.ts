import * as dotenv from 'dotenv';
dotenv.config();
import 'reflect-metadata';
import { createConnection, ConnectionOptions } from 'typeorm';
import express, { Express } from 'express';
import { setUpRoutes } from './routes';
import { seedDatabase } from './seed/seed';
import { setUpMiddlewares } from './middlewares';
import { seedTestDatabase } from '../test/seed/seedTest';
import databaseConf from '../ormconfig.json';
let databaseConfig: ConnectionOptions = databaseConf as ConnectionOptions;

const PORT = process.env.PORT;

const startExpressDevHttpsServer = (app: Express) => {
  // const https = require('https');
  // const fs = require('fs');
  // const serverRootPath = __dirname + '/../..';
  // const key = fs.readFileSync(serverRootPath + '/dev_cert/key.pem');
  // const cert = fs.readFileSync(serverRootPath + '/dev_cert/cert.pem');
  // const server = https.createServer({ key: key, cert: cert }, app);
  // server.listen(process.env.PORT_HTTPS, () => {
  //   console.log(
  //     `HTTPS dev server listening at https://localhost:${process.env.PORT_HTTPS}`
  //   );
  // });
};

const startExpressServer = (app: Express) => {
  if (process.env.NODE_ENV === 'dev') {
    startExpressDevHttpsServer(app);
  }
  app.listen(PORT, () => {
    console.log(`server started at http://localhost:${PORT}`);
  });
};

if (process.env.NODE_ENV === 'test') {
  databaseConfig = {
    ...databaseConfig,
    ssl: false,
    extra: null,
    url: process.env.DATABASE_TEST_URL,
    dropSchema: true,
  } as ConnectionOptions;
} else if (process.env.NODE_ENV === 'dev') {
  databaseConfig = {
    ...databaseConfig,
    ssl: false,
    extra: null,
    url: process.env.DATABASE_URL,
  } as ConnectionOptions;
} else {
  databaseConfig = {
    ...databaseConfig,
    url: process.env.DATABASE_URL,
  } as ConnectionOptions;
}

const setUpExpressAndStart = (
  serveNextAppCb?: (server: Express) => void
): void => {
  const app = express();
  setUpMiddlewares(app);
  setUpRoutes(app);
  startExpressServer(app);
  if (serveNextAppCb) serveNextAppCb(app);
};

createConnection(databaseConfig as ConnectionOptions)
  .then(async (connection) => {
    console.log('Database connected');

    if (process.env.NODE_ENV === 'test') await seedTestDatabase(connection);
    else await seedDatabase(connection);

    if (process.env.NODE_ENV === 'production') {
      /*
         #SERVE-NEXT-FRONTEND-START
          This serves next application in production.
          If the application would be split into two standalone servers (API and NEXT.js), then
          simply remove content of this condition and uninstall react, react-dom and next packages.
         */
      const next = require('next');
      const nextApp = next({ dev: false, dir: '../frontend' });
      const handle = nextApp.getRequestHandler();

      nextApp
        .prepare()
        .then(() => {
          setUpExpressAndStart((server: Express) => {
            server.use(express.static('../public'));
            server.get('*', (req, res) => {
              return handle(req, res);
            });
          });
        })
        .catch((ex: Error) => {
          console.error(ex.stack);
          process.exit(1);
        });
      //#SERVE-NEXT-FRONTEND-END
    } else {
      setUpExpressAndStart();
    }
  })
  .catch((error) => console.log(error));
