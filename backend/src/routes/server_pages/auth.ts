import { Request, Response, Router } from 'express';
import passport from 'passport';
import UserController from '../../controllers/UserController';

const router = Router();

const setCookieAndRedirect = (req: Request, res: Response): void => {
  res.cookie('token', req.user);
  res.redirect('/');
};

router.get(
  '/facebook',
  [],
  passport.authenticate('facebook', { scope: 'email' })
);

router.get(
  '/facebook/callback',
  [
    passport.authenticate('facebook', {
      session: false,
      failureRedirect: '/login',
    }),
  ],
  setCookieAndRedirect
);

router.get(
  '/google',
  [],
  passport.authenticate('google', {
    scope: [
      'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email',
    ],
  })
);

router.get(
  '/google/callback',
  [
    passport.authenticate('google', {
      session: false,
      failureRedirect: '/login',
    }),
  ],
  setCookieAndRedirect
);

router.get('/email/callback/:email/:token', [], UserController.verifyEmail);

export default router;
