import { Express, Router } from 'express';
import auth from './api/auth';
import user from './api/user';
import meal from './api/meal';
import restaurant from './api/restaurant';
import order from './api/order';
import pagesAuth from './server_pages/auth';

const routesApi = Router();
routesApi.use('/auth', auth);
routesApi.use('/users', user);
routesApi.use('/meals', meal);
routesApi.use('/restaurants', restaurant);
routesApi.use('/orders', order);

const routesPages = Router();
routesPages.use('/auth', pagesAuth);

export const setUpRoutes = (app: Express) => {
  app.use('/api', routesApi);
  app.use('/server_pages', routesPages);
};
