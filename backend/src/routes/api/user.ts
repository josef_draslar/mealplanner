import { Router } from 'express';
import UserController from '../../controllers/UserController';
import {
  checkSignedIn,
  checkSignedInAdmin,
  checkSignedInOwner,
} from '../../middlewares/checkRole';

const router = Router();

router.get('/', [checkSignedInOwner], UserController.getAll);

router.get('/orders', [checkSignedIn], UserController.getOrders);

router.post('/:id([0-9]+)', [checkSignedInOwner], UserController.blockUser);

router.get('/me', [checkSignedIn], UserController.me);

router.get('/:id([0-9]+)', [checkSignedIn], UserController.getById);

router.put('/:id([0-9]+)', [checkSignedIn], UserController.update);

router.delete('/:id([0-9]+)', [checkSignedInAdmin], UserController.deleteUser);

export default router;
