import { Router } from 'express';
import MealController from '../../controllers/MealController';
import { checkSignedInOwner } from '../../middlewares/checkRole';

const router = Router();

router.get('/', [], MealController.getAll);

router.get('/:id([0-9]+)', [], MealController.getById);

router.post('/', [checkSignedInOwner], MealController.create);

router.delete('/:id([0-9]+)', [checkSignedInOwner], MealController.delete);

router.put('/:id([0-9]+)', [checkSignedInOwner], MealController.update);

export default router;
