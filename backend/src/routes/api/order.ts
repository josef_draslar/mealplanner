import { Router } from 'express';
import MealController from '../../controllers/MealController';
import { checkSignedIn, checkSignedInOwner } from '../../middlewares/checkRole';
import OrderController from '../../controllers/OrderController';

const router = Router();

router.get('/:id([0-9]+)', [checkSignedIn], OrderController.getById);

router.post('/', [checkSignedIn], OrderController.create);

router.post(
  '/:id([0-9]+)/cancel',
  [checkSignedIn],
  OrderController.setStateCancel
);

router.post(
  '/:id([0-9]+)/next_state',
  [checkSignedInOwner],
  OrderController.setStateNextStateByOwner
);

router.post(
  '/:id([0-9]+)/received',
  [checkSignedIn],
  OrderController.setStateReceived
);

export default router;
