import { Router } from 'express';
import AuthController from '../../controllers/AuthController';
import { checkSignedIn } from '../../middlewares/checkRole';
import UserController from '../../controllers/UserController';

const router = Router();

router.post('/login', AuthController.login);

router.post('/reset-password', [], AuthController.resetPassword);

router.post('/set-new-password', [], AuthController.setNewPassword);

router.post('/change-password', [checkSignedIn], AuthController.changePassword);

router.post('/signup', [], UserController.signUp);

export default router;
