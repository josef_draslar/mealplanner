import { Router } from 'express';
import { checkSignedInOwner } from '../../middlewares/checkRole';
import RestaurantController from '../../controllers/RestaurantController';

const router = Router();

router.get('/', [], RestaurantController.getAll);

router.get('/:id([0-9]+)/meals', [], RestaurantController.getMeals);

router.get(
  '/:id([0-9]+)/orders',
  [checkSignedInOwner],
  RestaurantController.getOrders
);

router.get('/:id([0-9]+)', [], RestaurantController.getById);

router.post('/', [checkSignedInOwner], RestaurantController.create);

router.get(
  '/:id([0-9]+)/can_create_meals',
  [checkSignedInOwner],
  RestaurantController.canCreateMeals
);

router.delete(
  '/:id([0-9]+)',
  [checkSignedInOwner],
  RestaurantController.delete
);

router.put('/:id([0-9]+)', [checkSignedInOwner], RestaurantController.update);

export default router;
