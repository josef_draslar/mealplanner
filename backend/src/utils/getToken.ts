import * as jwt from 'jsonwebtoken';
import config from '../config/config';
import { User } from '../entities/User';

export const getToken = (user: User): string =>
  jwt.sign(
    { userId: user.id, email: user.email, role: user.role },
    config.jwtSecret,
    { expiresIn: '1h' }
  );
