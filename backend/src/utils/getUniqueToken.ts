import bcrypt from 'bcryptjs';

const getRandomString = () => {
  return Math.random().toString(36).substr(2); // remove `0.`
};

export const getUniqueToken = () =>
  encodeURI(bcrypt.hashSync(getRandomString() + getRandomString(), 8));
