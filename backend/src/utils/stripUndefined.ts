export const stripUndefined = (obj: Record<string, any> | undefined): void => {
    if (obj) {
        Object.keys(obj).forEach((key: string) => {
            if (obj[key] && typeof obj[key] === 'object') {
                stripUndefined(obj[key]);
            } else if (typeof obj[key] === 'undefined') {
                delete obj[key]; // eslint-disable-line no-param-reassign
            }
        });
    }
};
