export const validateEmail = (value: string): boolean => {
  const re = /^[a-zA-Z0-9][a-zA-Z0-9._+-]*(?<!\.)@[a-zA-Z0-9][a-zA-Z0-9-]*(\.[a-zA-Z0-9-]+)+(?<!\.)$/;
  const reDots = /\.\./;
  return re.test(value) && !reDots.test(value);
};

export const validatePassword = (value: string): string => {
  if (/[À-ž]/.test(value)) return 'signUp.diacriticsForbidden';
  if (!/[a-z]/.test(value)) return 'signUp.missingSmallLetter';
  if (!/[A-Z]/.test(value)) return 'signUp.missingBigLetter';
  if (!/[0-9]/.test(value)) return 'signUp.missingNumber';
  if (!/[\W|_]/.test(value)) return 'signUp.missingSpecialChar';
  if (value.length < 8) return 'signUp.atLeastEight';
  if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(\W|_)).{8,}$/.test(value))
    return 'signUp.passwordNotValid';

  return undefined;
};
