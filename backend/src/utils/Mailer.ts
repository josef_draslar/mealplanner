let mailer = require('@sendgrid/mail');
mailer.setApiKey(process.env.SENDGRID_API_KEY);

export const getMailer = () => mailer;
