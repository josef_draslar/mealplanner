import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { validate } from 'class-validator';
import { User } from '../entities/User';
import { getToken } from '../utils/getToken';
import { getUniqueToken } from '../utils/getUniqueToken';
import {
  getResetPasswordEmail,
  getVerifyEmail,
  sendEmail,
} from './EmailController';

class AuthController {
  static login = async (req: Request, res: Response) => {
    //Check if email and password are set
    let { email, password } = req.body;
    if (!(email && password)) {
      res.status(400).send();
      return;
    }

    //Get user from database
    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail({ where: { email } });
    } catch (error) {
      res.status(401).send();
      return;
    }

    //Check if encrypted password match
    if (!user.checkIfUnencryptedPasswordIsValid(password)) {
      res.status(401).send();
      return;
    }

    //Sing JWT, valid for 1 hour
    const token = getToken(user);

    //Send the jwt in the response
    res.send(token);
  };

  static changePassword = async (req: Request, res: Response) => {
    //Get ID from JWT
    const id = res.locals.jwtPayload.userId;

    //Get parameters from the body
    const { oldPassword, newPassword } = req.body;
    if (!(oldPassword && newPassword)) {
      res.status(400).send();
    }

    //Get user from the database
    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail(id);
    } catch (id) {
      res.status(401).send();
    }

    //Check if old password matchs
    if (!user.checkIfUnencryptedPasswordIsValid(oldPassword)) {
      res.status(401).send();
      return;
    }

    //Validate de model (password lenght)
    user.password = newPassword;
    const errors = await validate(user);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    //Hash the new password and save
    user.hashPassword();
    userRepository.save(user);

    res.status(204).send();
  };

  static resetPassword = async (req: Request, res: Response) => {
    let { email } = req.body;
    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail({
        where: { email },
      });
    } catch (e) {
      res.status(401).send('email not found');
    }

    const uniqueToken = getUniqueToken();

    user.emailToken = uniqueToken;

    try {
      await userRepository.save(user);
    } catch (e) {
      console.error(e);
      res.status(409).send('user not updated');
      return;
    }
    await sendEmail(getResetPasswordEmail(email, uniqueToken));

    res.status(200).send();
  };

  static setNewPassword = async (req: Request, res: Response) => {
    let { token, password } = req.body;
    if (!token || !password) {
      res.status(400).send();
    }
    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail({
        where: { emailToken: token },
      });
    } catch (e) {
      res.status(401).send('user not found');
    }

    user.password = password;
    user.emailToken = null;
    user.hashPassword();

    try {
      await userRepository.save(user);
    } catch (e) {
      console.error(e);
      res.status(409).send('user not saved');
      return;
    }
    res.status(200).send();
  };
}
export default AuthController;
