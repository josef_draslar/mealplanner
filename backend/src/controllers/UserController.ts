import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { validate } from 'class-validator';
import { RoleEnum, User } from '../entities/User';
import { validateEmail, validatePassword } from '../utils/validators';
import { RequestWithContext } from '../middlewares/setUserContext';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { getUniqueToken } from '../utils/getUniqueToken';
import { getVerifyEmail, sendEmail } from './EmailController';
import { Restaurant } from '../entities/Restaurant';

class UserController {
  static getAll = async (req: Request, res: Response) => {
    const userRepository = getRepository(User);
    const users = await userRepository.find({
      select: ['id', 'email', 'role'],
    });

    res.send(users);
  };

  static getById = async (req: RequestWithContext, res: Response) => {
    const id: number = parseInt(req.params.id, 10);
    if (
      req.context.user.id !== id &&
      req.context.user.role !== RoleEnum.ADMIN
    ) {
      res.status(401).send('NOT_AUTHORIZED');
      return;
    }

    const userRepository = getRepository(User);
    try {
      const user = await userRepository.findOneOrFail(id, {
        select: ['id', 'email', 'role'],
      });
      res.send(user);
    } catch (error) {
      res.status(404).send('User not found');
    }
  };

  static me = async (req: RequestWithContext, res: Response) => {
    const id: number = req.context.user.id;

    const userRepository = getRepository(User);
    try {
      const user = await userRepository.findOneOrFail(id, {
        select: ['id', 'email', 'role'],
        relations: ['restaurants', 'orders'],
      });
      res.send(user);
    } catch (error) {
      res.status(404).send('User not found');
    }
  };

  static signUp = async (req: Request, res: Response) => {
    let { email, password } = req.body;
    let user = new User();
    user.role = RoleEnum.USER;
    user.email = email;
    user.password = password;
    if (!user.password || !user.email) {
      res.status(400).send('missing fields');
      return;
    }
    if (!validateEmail(user.email)) {
      res.status(400).send('email invalid');
      return;
    }
    if (validatePassword(user.password)) {
      res.status(400).send(validatePassword(user.password));
      return;
    }
    const uniqueToken = getUniqueToken();

    user.emailToken = uniqueToken;
    user.hashPassword();

    if (process.env.NODE_ENV === 'test') {
      user.isVerified = true;
    }

    const userRepository = getRepository(User);
    let createdUser;
    try {
      createdUser = await userRepository.save(user);
    } catch (e) {
      console.error(e);
      res.status(409).send('email already in use');
      return;
    }

    if (process.env.NODE_ENV !== 'test') {
      await sendEmail(getVerifyEmail(email, uniqueToken));
    }

    delete createdUser.password;

    res.status(200).send({ id: createdUser.id });
  };

  static verifyEmail = async (req: Request, res: Response) => {
    let { email, token } = req.params;
    const userRepository = getRepository(User);

    const usersByEmailAndEmailToken: User[] = await userRepository.find({
      where: { email, emailToken: token },
      select: ['id', 'email', 'role'],
    });
    if (usersByEmailAndEmailToken.length > 0) {
      const user = usersByEmailAndEmailToken[0];
      user.emailToken = null;
      user.isVerified = true;
      await userRepository.save(user);

      res.redirect('/?message_key=Email%20verified&message_type=SUCCESS');
      return;
    }
    res.redirect(
      '/?message_key=Email%20verification&failes&message_type=ERROR'
    );
    return;
  };

  static getOrCreateUserServer = async (
    accessToken: string,
    refreshToken: string,
    email: string,
    googleId?: string
  ): Promise<User> => {
    const userRepository = getRepository(User);

    const where: FindConditions<User> = {};
    if (googleId) {
      where.googleId = googleId;
    } else {
      where.accessToken = accessToken;
    }
    const usersByKey: User[] = await userRepository.find({
      where,
      select: ['id', 'email', 'role'],
    });
    if (usersByKey.length > 0) {
      return usersByKey[0];
    }

    const users: User[] = await userRepository.find({
      where: { email },
      select: ['id', 'email', 'role'],
    });
    if (users.length > 0) {
      const user = users[0];
      if (googleId) {
        user.googleId = googleId;
        await userRepository.save(user);
      } else {
        user.accessToken = accessToken;
        await userRepository.save(user);
      }
      return user;
    }

    let user = new User();
    user.email = email;
    user.role = RoleEnum.USER;
    if (googleId) {
      user.googleId = googleId;
    } else {
      user.accessToken = accessToken;
      user.refreshToken = refreshToken;
    }
    return await userRepository.save(user);
  };

  static update = async (req: RequestWithContext, res: Response) => {
    const id: number = parseInt(req.params.id, 10);

    const { email } = req.body;

    if (
      req.context.user.id !== id &&
      req.context.user.role !== RoleEnum.ADMIN
    ) {
      res.status(401).send('NOT_AUTHORIZED');
      return;
    }
    if (!validateEmail(email)) {
      res.status(400).send('email invalid');
      return;
    }

    const userRepository = getRepository(User);
    let user;
    try {
      user = await userRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send('User not found');
      return;
    }

    user.email = email;
    const errors = await validate(user);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    try {
      await userRepository.save(user);
    } catch (e) {
      res.status(409).send('email already in use');
      return;
    }

    res.status(204).send();
  };

  static getOrders = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    const id = req.context.user.id;
    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail(id, {
        relations: ['orders'],
      });
    } catch (e) {
      res.status(400).send('wrong user id');
    }

    res.status(200).send(user.orders);
  };

  static blockUser = async (req: Request, res: Response) => {
    const id = req.params.id;

    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send('User not found');
      return;
    }

    user.blocked = true;

    try {
      await userRepository.save(user);
    } catch (error) {
      res.status(400).send('User blocking failed');
      return;
    }

    res.status(204).send();
  };

  static deleteUser = async (req: Request, res: Response) => {
    const id = req.params.id;

    const userRepository = getRepository(User);
    try {
      await userRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send('User not found');
      return;
    }
    await userRepository.delete(id);

    res.status(204).send();
  };
}

export default UserController;
