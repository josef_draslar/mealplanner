import { Request, Response } from 'express';
import { getManager, getRepository } from 'typeorm';
import { validate } from 'class-validator';
import { stripUndefined } from '../utils/stripUndefined';
import { RequestWithContext } from '../middlewares/setUserContext';
import { Restaurant } from '../entities/Restaurant';
import { Meal } from '../entities/Meal';
import { User } from '../entities/User';

const getRestaurantFromRequest = (req: Request): Restaurant => {
  let { name, description } = req.body;
  let restaurant = new Restaurant();
  restaurant.name = name;
  restaurant.description = description;
  stripUndefined(restaurant);
  return restaurant;
};

export const getIdFromRequest = (req: Request): number =>
  parseInt(req.params.id, 10);

export default class RestaurantController {
  static getAll = async (req: Request, res: Response): Promise<void> => {
    const restaurantRepository = getRepository(Restaurant);
    const restaurants = await restaurantRepository.find({
      where: { deprecated: false },
    });

    res.send(restaurants);
  };

  static getOrders = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    const id = req.params.id;
    const restaurantRepository = getRepository(Restaurant);
    let restaurant: Restaurant;
    try {
      restaurant = await restaurantRepository.findOneOrFail(id, {
        relations: ['orders', 'orders.mealOrders', 'orders.mealOrders.meal'],
      });
    } catch (e) {
      res.status(400).send('wrong restaurant id');
    }
    if (restaurant.userId !== req.context.user.id) {
      res.status(403).send();
    }

    res.status(200).send(restaurant.orders);
  };

  static canCreateMeals = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    const id = req.params.id;
    const restaurantRepository = getRepository(Restaurant);
    let restaurant: Restaurant;
    try {
      restaurant = await restaurantRepository.findOneOrFail(id, {
        relations: ['user'],
      });
    } catch (e) {
      res.status(400).send('wrong restaurant id');
    }

    if (restaurant.user.id !== req.context.user.id) {
      res.status(403).send();
    }

    res.status(201).send();
  };

  static getMeals = async (req: Request, res: Response): Promise<void> => {
    const id = req.params.id;
    const restaurantRepository = getRepository(Restaurant);
    let restaurant: Restaurant;
    try {
      restaurant = await restaurantRepository.findOneOrFail(id, {
        relations: ['meals'],
      });
    } catch (e) {
      res.status(400).send('wrong restaurant id');
    }

    res.status(200).send(restaurant.meals.filter((meal) => !meal.deprecated));
  };

  static getById = async (req: Request, res: Response): Promise<void> => {
    const restaurantRepository = getRepository(Restaurant);
    try {
      const restaurant = await restaurantRepository.findOneOrFail(
        req.params.id
      );
      res.status(200).send(restaurant);
    } catch (e) {
      res.status(404).send();
    }
  };

  static create = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    const restaurant = getRestaurantFromRequest(req);

    const errors = await validate(restaurant);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    restaurant.userId = req.context.user.id;

    try {
      const mealRepository = getRepository(Restaurant);
      await mealRepository.save(restaurant);
      res.status(201).send();
    } catch (e) {
      console.error(e);
      res.status(409).send('Restaurant creation failed');
      return;
    }
  };

  static delete = async (req: Request, res: Response): Promise<void> => {
    const id: number = getIdFromRequest(req);
    const restaurantRepository = getRepository(Restaurant);
    let restaurant: Restaurant;
    try {
      restaurant = await restaurantRepository.findOneOrFail(id, {
        relations: ['orders', 'meals'],
      });
    } catch (error) {
      res.status(404).send('Restaurant not found');
      return;
    }

    try {
      if (restaurant.orders.length === 0) {
        await getManager().transaction(async () => {
          const mealRepository = getRepository(Meal);
          for (const meal of restaurant.meals) {
            await mealRepository.delete(meal.id);
          }
          await restaurantRepository.delete(id);

          res.status(204).send();
        });
      } else {
        restaurant.deprecated = true;
        await restaurantRepository.save(restaurant);
      }
      res.status(204).send();
    } catch (error) {
      res.status(400).send('Restaurant not deleted, error occured');
      return;
    }
  };

  static update = async (req: Request, res: Response): Promise<void> => {
    const id: number = getIdFromRequest(req);

    const restaurantRepository = getRepository(Restaurant);
    let restaurantFromDb;
    try {
      restaurantFromDb = await restaurantRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send('Restaurant not found');
      return;
    }

    const restaurantFromRequest = getRestaurantFromRequest(req);
    const errors = await validate(restaurantFromRequest);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    restaurantFromDb.name = restaurantFromRequest.name;
    restaurantFromDb.description = restaurantFromRequest.description;

    try {
      await restaurantRepository.save(restaurantFromDb);
      res.status(204).send();
    } catch (e) {
      res.status(400).send('restaurant not saved');
      return;
    }
  };
}
