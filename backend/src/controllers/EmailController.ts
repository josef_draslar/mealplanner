import { getMailer } from '../utils/Mailer';
import { MailDataRequired } from '@sendgrid/helpers/classes/mail';
import { AxiosError } from 'axios';

export const getResetPasswordEmail = (
  email: string,
  token: string
): MailDataRequired => ({
  from: 'josef.app.test@gmail.com',
  subject: 'Reset password',
  text: 'click here to reset password',
  html: `<div>click <a href="${
    process.env.DOMAIN
  }/?open_modal=RESET_PASSWORD&token=${encodeURIComponent(
    token
  )}">here</a> to reset password</div>`,
  to: email,
});

export const getVerifyEmail = (
  email: string,
  token: string
): MailDataRequired => ({
  from: 'josef.app.test@gmail.com',
  subject: 'Verify email',
  text: 'verify email',
  html: `<div><a href="${
    process.env.DOMAIN
  }/server_pages/auth/email/callback/${email}/${encodeURIComponent(
    token
  )}">verify email</a></div>`,
  to: email,
});

export const sendEmail = async (mail: MailDataRequired) => {
  await getMailer()
    .send(mail)
    .then(() => {
      console.log('Email sent');
    })
    .catch((error: AxiosError) => {
      //@ts-ignore-next-line
      console.error(error, error.response.body.errors);
    });
};
