import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Meal } from '../entities/Meal';
import { validate } from 'class-validator';
import { stripUndefined } from '../utils/stripUndefined';
import { RequestWithContext } from '../middlewares/setUserContext';
import { Restaurant } from '../entities/Restaurant';

const getMealFromRequest = (req: Request): Meal => {
  let { name, description, price } = req.body;
  let meal = new Meal();
  meal.name = name;
  meal.description = description;
  meal.price = price;
  stripUndefined(meal);
  return meal;
};

export const getIdFromRequest = (req: Request): number =>
  parseInt(req.params.id, 10);

export default class MealController {
  static getAll = async (req: Request, res: Response): Promise<void> => {
    const mealRepository = getRepository(Meal);
    const meals = await mealRepository.find({ where: { deprecated: false } });

    res.send(meals);
  };

  static getById = async (req: Request, res: Response): Promise<void> => {
    const mealRepository = getRepository(Meal);
    try {
      const meal = await mealRepository.findOneOrFail(req.params.id);
      res.status(200).send(meal);
    } catch (e) {
      res.status(404).send();
    }
  };

  static create = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    let { restaurant_id } = req.body;
    const meal = getMealFromRequest(req);

    const restaurantRepository = getRepository(Restaurant);
    let restaurant: Restaurant;
    try {
      restaurant = await restaurantRepository.findOneOrFail(restaurant_id);
    } catch (e) {
      res.status(400).send('wrong restaurant id');
    }
    if (restaurant.userId != req.context.user.id) {
      res.status(403).send();
    }

    meal.restaurant = restaurant;

    const errors = await validate(meal);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    try {
      const mealRepository = getRepository(Meal);
      await mealRepository.save(meal);
      res.status(201).send('Meal created');
    } catch (e) {
      res.status(409).send('Meal creation failed');
      return;
    }
  };

  static delete = async (req: Request, res: Response): Promise<void> => {
    const id: number = getIdFromRequest(req);
    const mealRepository = getRepository(Meal);
    let meal: Meal;
    try {
      meal = await mealRepository.findOneOrFail(id, {
        relations: ['mealOrders'],
      });
    } catch (error) {
      res.status(404).send('Meal not found');
      return;
    }

    try {
      if (meal.mealOrders.length === 0) {
        await mealRepository.delete(id);
      } else {
        meal.deprecated = true;
        await mealRepository.save(meal);
      }
      res.status(204).send();
    } catch (error) {
      res.status(400).send('Meal not deleted, error occured');
      return;
    }
  };

  static update = async (req: Request, res: Response): Promise<void> => {
    const id: number = getIdFromRequest(req);

    const mealRepository = getRepository(Meal);
    let meal: Meal;
    try {
      meal = await mealRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send('Meal not found');
      return;
    }

    const mealFromRequest = getMealFromRequest(req);
    const errors = await validate(mealFromRequest);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    meal.name = mealFromRequest.name;
    meal.price = mealFromRequest.price;
    meal.description = mealFromRequest.description;

    try {
      await mealRepository.save(meal);
      res.status(204).send();
    } catch (e) {
      res.status(400).send('meal not saved');
      return;
    }
  };
}
