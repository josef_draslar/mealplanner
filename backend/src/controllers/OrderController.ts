import { Request, Response } from 'express';
import { getManager, getRepository, In } from 'typeorm';
import { Order, OrderStateEnum } from '../entities/Order';
import { RequestWithContext } from '../middlewares/setUserContext';
import { Meal } from '../entities/Meal';
import { MealOrder } from '../entities/MealOrder';
import { User } from '../entities/User';
import restaurant from '../routes/api/restaurant';

interface MealOrderCreated {
  id: number;
  amount: number;
  price: number;
}

export const getIdFromRequest = (req: Request): number =>
  parseInt(req.params.id, 10);

export default class OrderController {
  static getById = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    const orderRepository = getRepository(Order);
    try {
      const order = await orderRepository.findOneOrFail(req.params.id, {
        relations: ['user'],
      });

      if (order.user.id != req.context.user.id) {
        res.status(403).send();
        return;
      }

      res.status(200).send(order);
    } catch (e) {
      res.status(404).send();
    }
  };

  static create = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    let {
      meals,
      restaurantId,
    }: { meals: MealOrder[]; restaurantId: number } = req.body;
    const mealIds: number[] = meals.map((meal: MealOrder) => meal.id);
    console.log('mealIds', mealIds);
    const mealRepository = getRepository(Meal);
    const mealsFromDb = await mealRepository.find({
      where: { id: In(mealIds) },
    });

    const mealsLocal: MealOrderCreated[] = meals
      .map((meal) => {
        const mealFromDb = mealsFromDb.find(
          (mealFromDb) => mealFromDb.id == meal.id
        );
        if (!mealFromDb) return undefined;
        return {
          id: Number(meal.id),
          amount: Number(meal.amount),
          price: mealFromDb.price,
          meal: mealFromDb,
        };
      })
      .filter((meal) => !!meal);

    if (mealsLocal.length !== meals.length) {
      res
        .status(400)
        .send(
          'some of the meals were not found, please refresh and place your order again'
        );
      return;
    }
    let total = 0;
    mealsLocal.forEach((meal) => (total += meal.amount * meal.price));

    const order = new Order();
    order.placed = new Date();
    order.total = total;
    order.status = OrderStateEnum.PLACED;
    order.userId = req.context.user.id;
    order.restaurantId = restaurantId;

    const orderRepository = getRepository(Order);
    try {
      const orderSaved = await orderRepository.save(order);

      const mealOrderRepository = getRepository(MealOrder);
      for (const meal of mealsLocal) {
        await mealOrderRepository.save({
          meal: meal,
          order: orderSaved,
          amount: meal.amount,
        });
      }

      res.status(201).send();
    } catch (error) {
      console.error('error', error);
      res.status(400).send('Order not created');
      return;
    }
  };

  static setStateCancel = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    const id: number = getIdFromRequest(req);
    const orderRepository = getRepository(Order);
    let order: Order;
    try {
      order = await orderRepository.findOneOrFail(id, {
        relations: ['user'],
      });
    } catch (error) {
      res.status(404).send('Order not found');
      return;
    }

    if (order.user.id != req.context.user.id) {
      res.status(403).send();
      return;
    }

    if (order.status !== OrderStateEnum.PLACED) {
      res.status(400).send('status change not valid');
      return;
    }

    order.status = OrderStateEnum.CANCELED;
    order.canceled = new Date();

    try {
      await orderRepository.save(order);
      res.status(204).send();
    } catch (error) {
      res.status(400).send('Order not saved');
      return;
    }
  };

  static setStateReceived = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    const id: number = getIdFromRequest(req);
    const orderRepository = getRepository(Order);
    let order: Order;
    try {
      order = await orderRepository.findOneOrFail(id, {
        relations: ['user'],
      });
    } catch (error) {
      res.status(404).send('Order not found');
      return;
    }

    if (order.user.id != req.context.user.id) {
      res.status(403).send();
      return;
    }

    if (order.status !== OrderStateEnum.DELIVERED) {
      res.status(400).send('status change not valid');
      return;
    }

    order.status = OrderStateEnum.RECEIVED;
    order.received = new Date();

    try {
      await orderRepository.save(order);
      res.status(204).send();
    } catch (error) {
      res.status(400).send('Order not saved');
      return;
    }
  };

  static setStateNextStateByOwner = async (
    req: RequestWithContext,
    res: Response
  ): Promise<void> => {
    const id: number = getIdFromRequest(req);
    const orderRepository = getRepository(Order);
    const userRepository = getRepository(User);
    let order: Order;
    let user: User;
    try {
      order = await orderRepository.findOneOrFail(id, {
        relations: ['restaurant'],
      });
      user = await userRepository.findOneOrFail(req.context.user.id, {
        relations: ['restaurants'],
      });
    } catch (error) {
      res.status(404).send('Order not found');
      return;
    }

    if (
      !user.restaurants.some(
        (restaurant) => restaurant.id === order.restaurant.id
      )
    ) {
      res.status(403).send();
      return;
    }

    if (
      ![
        OrderStateEnum.PLACED,
        OrderStateEnum.PROCESSING,
        OrderStateEnum.IN_ROUTE,
      ].includes(order.status)
    ) {
      res.status(400).send('status change not valid');
      return;
    }

    if (order.status === OrderStateEnum.PLACED) {
      order.status = OrderStateEnum.PROCESSING;
      order.processing = new Date();
    } else if (order.status === OrderStateEnum.PROCESSING) {
      order.status = OrderStateEnum.IN_ROUTE;
      order.in_route = new Date();
    } else {
      order.status = OrderStateEnum.DELIVERED;
      order.delivered = new Date();
    }

    try {
      await orderRepository.save(order);
      res.status(204).send();
    } catch (error) {
      res.status(400).send('Order not saved');
      return;
    }
  };
}
