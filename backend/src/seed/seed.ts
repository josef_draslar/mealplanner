import { Connection } from 'typeorm/connection/Connection';
import { RoleEnum, User } from '../entities/User';

export const ADMIN = {
  email: 'admin@test.cz',
  role: RoleEnum.ADMIN,
  password: 'test',
};

const OWNER = {
  email: 'owner@test.cz',
  role: RoleEnum.OWNER,
  password: 'test',
};

const USER = {
  email: 'user@test.cz',
  role: RoleEnum.USER,
  password: 'test',
};

async function createUser(
  connection: Connection,
  userParams: User,
  name: string
) {
  const userRepository = connection.getRepository(User);
  const existingUser = await userRepository.findOne(undefined, {
    where: { email: userParams.email },
  });
  if (!existingUser) {
    const user = new User();
    user.email = userParams.email;
    user.password = userParams.password;
    user.role = userParams.role;
    user.hashPassword();
    const createdUser = await userRepository.save(user);
    console.log(name + ' created');
    return createdUser.id;
  }
  return existingUser.id;
}

export const seedDatabase = async (connection: Connection) => {
  await createUser(connection, ADMIN as User, 'admin');
  await createUser(connection, OWNER as User, 'owner');
  await createUser(connection, USER as User, 'user');
};
