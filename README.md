(
postgres
brew tap dbcli/tap
brew install pgcli
brew tap heroku/brew && brew install heroku
)

#INSTALLATION
brew
mysql
node
yarn

DEV ENV SET UP
intellij idea - plugin (prettier, env), set prettier on save

yarn install

# BACKEND
yarn dev
yarn test




# FRONTEND

#prerequisities
yarn
node
create-next-app



# Next.js 10 with SCSS
```
yarn create next-app frontend
```
```
cd frontend
```
```
touch tsconfig.json
```
```
yarn add --dev typescript @types/react @types/node
```
rename files with .jsx to .tsx under /pages
```
yarn add sass @zeit/next-sass
```
# RUN THE APP
```
yarn dev
```
#set up eslint
```
nano .eslintignore
```
paste there and save
```
/public
/logs
/docker
/.next
/node_modules
```
```
nano .eslintrc.js
```
paste there and save
```
module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: { jsx: true },
    },
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:react/recommended',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
        'prettier/@typescript-eslint',
        'plugin:prettier/recommended',
    ],
    rules: {
        'react/prop-types': 'off',
        'jsx-a11y/click-events-have-key-events': 'off',
        'jsx-a11y/no-static-element-interactions': 'off',
        'react/display-name': 'off',
        'import/order': [
            'error',
            {
                pathGroups: [
                    {
                        pattern: './*.scss',
                        group: 'internal',
                        position: 'after',
                    },
                ],
            },
        ],
        'prettier/prettier': ['error', {}, { usePrettierrc: true }],
        'import/no-named-as-default': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/ban-ts-comment': 'off',
    },
    env: { commonjs: true },
    settings: {
        react: {
            version: 'detect',
        },
        'import/resolver': {
            node: {
                paths: ['.'],
            },
        },
    },
};
```
#set up prettier
```
nano .prettierrrc.js
```
paste there and save
```
module.exports = {
    semi: true,
    trailingComma: 'es5',
    singleQuote: true,
}
```
#set up stylelint
```
nano .stylelintrc.json
```
paste there and save
```
{
    "extends": [
        "stylelint-config-sass-guidelines",
        "stylelint-config-rational-order"
    ],
    "rules": {
        "property-no-vendor-prefix": [
            true,
            {
                "ignoreProperties": [
                    "appearance"
                ]
            }
        ],
        "unit-allowed-list": [
            ["em", "rem", "%", "s", "px"],
            {
                "ignoreProperties": {
                    "px": [
                        "border",
                        "border-bottom",
                        "border-top",
                        "border-left",
                        "border-right",
                        "box-shadow",
                        "height",
                        "min-height",
                        "max-height",
                        "width",
                        "min-width",
                        "max-width",
                        "stroke-width"
                    ],
                    "vh": ["height", "min-height", "max-height"],
                    "vw": ["width", "min-width", "max-width"],
                    "deg": ["transform"]
                }
            }
        ],
        "max-nesting-depth": 2,
        "selector-class-pattern": "[a-zA-z]",
        "scss/dollar-variable-pattern": "[a-zA-z]",
        "selector-no-qualifying-type": [
            true,
            {
                "ignore": ["class", "attribute"]
            }
        ]
    }
}
```
#modify ts.config.json with this
```
{
  "compilerOptions": {
    "lib": [
      "dom",
      "dom.iterable",
      "esnext"
    ],
    "allowJs": true,
    "skipLibCheck": true,
    "forceConsistentCasingInFileNames": true,
    "plugins": [
      {
        "name": "typescript-plugin-css-modules"
      }
    ],
    "noEmit": true,
    "esModuleInterop": true,
    "moduleResolution": "node",
    "resolveJsonModule": true,
    "isolatedModules": true,
    "module": "esnext",
    "target": "es2015",
    "jsx": "preserve",
    "strict": true
  },
  "include": [
    "next-env.d.ts",
    "**/*.ts",
    "**/*.tsx"
  ],
  "exclude": [
    "node_modules"
  ]
}

```
#create ts.config.dev.json with this
```
nano ts.config.dev.json
```
paste there and save
```
{
  "extends": "./tsconfig.json",
  "compilerOptions": {
    "jsx": "react-jsxdev"
  }
}
```
add these LINTING dependencies
```
    "typescript-plugin-css-modules": "^3.2.0"
```
add dependency for typescript css modules (to by able to use SCSS at component level)
```
    "typescript-plugin-css-modules": "^3.2.0"
```
add this to package.json
```
,
  "lint-staged": {
    "*.{ts,tsx}": [
      "prettier --write"
    ],
    "*.scss": [
      "stylelint --syntax scss --fix"
    ]
  }
```
add these scripts to package.json
```
    "precommit": "lint-staged",
    "lint": "./node_modules/.bin/eslint --fix */**/*.{js,ts,tsx}",
```
add axios
```
yarn add axios
```
set up dev server redirect
```
mkdir utils
nano utils/Axios.ts
```
pasta there and save
```
import axiosLib from 'axios';

const axiosConfig = {
  baseURL:
    (process.env.NODE_ENV !== 'production' ? 'http://localhost:7000' : '') +
    '/api',
  timeout: 5000,
};

export const axios = axiosLib.create(axiosConfig);
```

#RUNNING APP ON ONE SERVER (HEROKU)
add to package.json in root these scripts
```
"_install-backend": "cd backend && yarn install --only=prod",
    "_install-frontend": "cd frontend && yarn install --only=prod",
    "heroku-postbuild": "yarn _install-backend && yarn _install-frontend && yarn _clean && yarn _build-next && yarn _copy-next",
    "start": "cd backend && yarn start",
    "_clean": "rimraf ./backend/.next",
    "_build-next": "cd frontend && yarn build",
    "_copy-next": "cp -r ./frontend/.next/ ./backend/.next",
    "_run-server": "cd backend && yarn start-as-production",
    "run-production": "yarn _clean && yarn _build-next && yarn _copy-next && yarn _run-server",
```
do ``yarn run-production`` to run it locally
``start`` and ``heroku-postbuild`` are triggered by heroku itself while deploying

# GIT
```
git init
```
```
git remote add origin YOUR_ORIGIN_URL
```
```
git add .
```
```
git commit -am "init repo"
```
```
git push -u origin master
```



#set up heroku app
create app in heroku
heroku git:remote -a josef-mealplanner
git remote rename heroku heroku-prod
git push heroku-prod master


# Next.js 10 with STYLED-COMPONENTS
`yarn create next-app --example with-styled-components frontend`
`cd frontend`
`touch tsconfig.json`
`yarn add --dev typescript @types/react @types/node`
rename files with .jsx to .tsx under /pages
`yarn dev`
install styled components plugin in your IDE






`openssl req -x509 -newkey rsa:2048 -keyout keytmp.pem -out cert.pem -days 365`
`openssl rsa -in keytmp.pem -out key.pem`